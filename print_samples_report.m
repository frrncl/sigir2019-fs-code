%% print_sig_tests_report
% 
% Reports a summary about the ANOVA analyses and saves them to a |.tex|
% file.
%
%% Synopsis
%
%   [] = print_sig_tests_report(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros; 
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use q1 for top
% quartile; q2 for median; q3 for up to third quartile; q4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = print_samples_report(trackID, splits, balanced, sstype, quartile, startMeasure, endMeasure)

    persistent MD;

    if isempty(MD)
        MD = 'md3';
    end

    % check the number of input parameters
    narginchk(5, 7);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');

    for k = 1:length(splits)
        splitID = splits{k};
        
        % check that splitID is a non-empty cell array
        validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
        
        if iscell(splitID)
            % check that splitID is a cell array of strings with one element
            assert(iscellstr(splitID) && numel(splitID) == 1, ...
                'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
        end
        
        % remove useless white spaces, if any, and ensure it is a char row
        splitID = char(strtrim(splitID));
        splitID = splitID(:).';
        
        % check that splitID assumes a valid value
        validatestring(splitID, ...
            EXPERIMENT.split.list, '', 'splitID');
        
        % check that the track and the split rely on the same corpus
        assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);
    end
    
    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');

    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';

    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');

    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
        {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');

    % check that quartile is a non-empty cell array
    validateattributes(quartile, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'quartile');

    if iscell(quartile)
        % check that quartile is a cell array of strings with one element
        assert(iscellstr(quartile) && numel(quartile) == 1, ...
            'MATTERS:IllegalArgument', 'Expected quartile to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    quartile = char(strtrim(quartile));
    quartile = quartile(:).';

    % check that quartile assumes a valid value
    validatestring(quartile, ...
        EXPERIMENT.analysis.quartile.list, '', 'quartile');


    if nargin == 7
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');

        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;


    fprintf('\n\n######## Reporting summary ANOVA analyses on track %s (%s) ########\n\n', ...
        trackID, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %s - %s \n', quartile, EXPERIMENT.analysis.quartile.(quartile).description);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', join(string(splits), ", "));
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));            

        
    fprintf('+ Printing the report\n');
    fprintf('  ');
    
    % the file where the report has to be written
    reportID = EXPERIMENT.pattern.identifier.report.samples(balanced, sstype, quartile, join(string(splits), "_"), trackID);
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, reportID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{longtable}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    
    fprintf(fid, '\\title{Report on Samples on %s}\n\n', ...
        strrep(trackID, '_', '\_'));
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
        
  
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
        
        tau.values = NaN(length(splits), EXPERIMENT.split.sample);
        
        tukeyCI.values = NaN(length(splits), EXPERIMENT.split.sample);
        
        sigPairs.values = NaN(length(splits), EXPERIMENT.split.sample);
        sigPairs.pair = NaN(length(splits), 1);
        
        for k = 1:length(splits)
            splitID = splits{k};
            
            tmp = [];
            
            for s = 1:EXPERIMENT.split.sample
                
                % load MD data
                anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MD, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, s), trackID);                
                anovaMeID = EXPERIMENT.pattern.identifier.anova.me(MD, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, s), trackID);
                anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(MD, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, s), trackID);
                
                serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, splitID, anovaID), ...
                    'WorkspaceVarNames', {'md_me', 'md_mc'}, ...
                    'FileVarNames', {anovaMeID, anovaMcID});
                
                tau.values(k, s) = md_me.factorA.tau;
                
                tukeyCI.values(k, s) = md_me.factorA.tukey.halfWidth * 2;
                
                sig = md_mc(:, 6) <= EXPERIMENT.analysis.alpha.threshold;
                tmp = [tmp sig];
                
                sigPairs.values(k, s) =  sum(sig);
                
                sigPairs.number = size(md_mc, 1);
                
                clear md_me md_mc
                
            end % for samples
            
           % sigPairs.pair(k) = mean(sum(tmp, 2)/EXPERIMENT.split.sample);
           sigPairs.pair(k) = mean(sum(tmp, 2))/EXPERIMENT.split.sample;
            
        end % for splits
        
        tau.mean = mean(tau.values, 2);
        tau.ci = tinv(1 - EXPERIMENT.analysis.alpha.threshold/2, EXPERIMENT.split.sample - 1) * std(tau.values, 0, 2) / sqrt(EXPERIMENT.split.sample);
        
        tukeyCI.mean = mean(tukeyCI.values, 2);
        tukeyCI.ci = tinv(1 - EXPERIMENT.analysis.alpha.threshold/2, EXPERIMENT.split.sample - 1) * std(tukeyCI.values, 0, 2) / sqrt(EXPERIMENT.split.sample);
        
        sigPairs.mean = mean(sigPairs.values, 2);
        sigPairs.ci = tinv(1 - EXPERIMENT.analysis.alpha.threshold/2, EXPERIMENT.split.sample - 1) * std(sigPairs.values, 0, 2) / sqrt(EXPERIMENT.split.sample);
        
        fprintf(fid, '\\begin{table*}[tbp] \n');
        
        fprintf(fid, '\\centering \n');
                
        fprintf(fid, '\\caption{Summary of analyses for %s on track \\texttt{%s} using %d samples of each random split . Model~\\eqref{eq:%s}; %d system pairs compared.}\n', ...
            strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), ...
            strrep(trackID, '_', '\_'), EXPERIMENT.split.sample, ...
            MD, sigPairs.number);
        
        fprintf(fid, '\\label{tab:smrysigcnts-%s-%s-%s}\n', mid, trackID, splitID);
        fprintf(fid, '\\small \n');
        fprintf(fid, '\\begin{tabular}{|l|r|r|r|r|} \n');
        
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c|}{\\textbf{Split}} & \\multicolumn{1}{c|}{\\textbf{Kendal''s $\\tau$}} & \\multicolumn{1}{c|}{\\textbf{Tukey CI Width}} & \\multicolumn{1}{c|}{\\textbf{Total Sig. Pairs}} & \\multicolumn{1}{c|}{\\textbf{Same Sig Pair Over Shards Ratio}} \\\\ \n');
        
        fprintf(fid, '\\hline \n');
        
        for k = 1:length(splits)
            splitID = splits{k};
        
             fprintf(fid, '\\texttt{%s} & %5.4f $\\pm$ %5.4f & %8.4f $\\pm$ %8.4f & %8.2f $\\pm$ %8.2f & %8.4f \\\\ \n', ...
                strrep(splitID, '_', '\_'), ...
                tau.mean(k), tau.ci(k), ...
                tukeyCI.mean(k), tukeyCI.ci(k), ...
                sigPairs.mean(k), sigPairs.ci(k), ...
                sigPairs.pair(k));
            
             fprintf(fid, '\\hline \n');
        end
                   
        fprintf(fid, '\\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');
        
        fprintf(fid, '\\end{table*} \n\n');
        
        
    end
                                       
    fprintf(fid, '\\end{document} \n\n');
        
    fclose(fid);
                       
    fprintf('\n\n######## Total elapsed time for reporting summary ANOVA analyses (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

        
end
