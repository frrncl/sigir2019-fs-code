Matlab source code for running the experiments reported in the paper:

* Ferro, N. and Sanderson, M. (2019). Improving the Accuracy of System Performance Estimation by Using Shards. _Proc. 42nd Annual International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR 2019)_.
