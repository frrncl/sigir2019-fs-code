%% plot_main_effects_balancing
% 
% Plots the main effects of the system factor for the different balancing
% approaches, comparing it to the whole corpus.

%% Synopsis
%
%   [] = plot_main_effects_balancing(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|balanced|* - a cell array with the type of balancing. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros; 
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use q1 for top
% quartile; q2 for median; q3 for up to third quartile; q4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = plot_main_effects_balancing(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)

    persistent MD1 MD2 ALPHA;

    if isempty(RQ0)
        RQ1 = 'rq1';
        RQ2 = 'rq1';
        ALPHA = 0.2
    end

    % check the number of input parameters
    narginchk(5, 7);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');

    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');

    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';

    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');

    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);

    % check the different balancing strategies
    for i = 1:length(balanced)        
        blc = balanced{i};
        
        % check that balanced is a non-empty cell array
        validateattributes(blc, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
        
        if iscell(blc)
            % check that balanced is a cell array of strings with one element
            assert(iscellstr(blc) && numel(blc) == 1, ...
                'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
        end
        
        % check that balanced assumes a valid value
        validatestring(blc, ...
            EXPERIMENT.analysis.balanced.list, '', 'balanced');        
    end
    
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
        {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');

    % check that quartile is a non-empty cell array
    validateattributes(quartile, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'quartile');

    if iscell(quartile)
        % check that quartile is a cell array of strings with one element
        assert(iscellstr(quartile) && numel(quartile) == 1, ...
            'MATTERS:IllegalArgument', 'Expected quartile to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    quartile = char(strtrim(quartile));
    quartile = quartile(:).';

    % check that quartile assumes a valid value
    validatestring(quartile, ...
        EXPERIMENT.analysis.quartile.list, '', 'quartile');


    if nargin == 7
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');

        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting main effects of ANOVA analyses on track %s (%s) ########\n\n', ...
        trackID, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * balanced: %s\n', join(string(balanced), ", "));
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %s - %s \n', quartile, EXPERIMENT.analysis.quartile.(quartile).description);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));            

    
     % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Plotting effects of %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
        
        % load the main effects on the whole corpus
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(RQ0, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID).corpus, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(RQ0, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID).corpus, trackID);
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
            'WorkspaceVarNames', {'rq0_me'}, ...
            'FileVarNames', {anovaMeID});
        
        rq1_me = cell(1, length(balanced));
        rq1_blc = cell(1, length(balanced));
        
        % for each balancing approach, load the corresponding main effects
        for i = 1:length(balanced)
            anovaID = EXPERIMENT.pattern.identifier.anova.analysis(RQ1, balanced{i}, sstype, quartile, mid, splitID, trackID);
            anovaMeID = EXPERIMENT.pattern.identifier.anova.me(RQ1, balanced{i}, sstype, quartile, mid, splitID, trackID);
            anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(RQ1, balanced{i}, sstype, quartile, mid, splitID, trackID);
            
            serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'tmp', 'tmp1'}, ...
                'FileVarNames', {anovaMeID, anovaBlcID});
            
            rq1_me{i} = tmp;
            rq1_blc{i} = tmp1;
        end
        
        clear tmp tmp1
        
        for i = 1:length(balanced)-1
            assert(rq1_me{i}.factorA.tau ==  rq1_me{i+1}.factorA.tau, 'Correlation between %d and %d not equal: %5.4f vs %5.4f', i, i+1, rq1_me{i}.factorA.tau, rq1_me{i+1}.factorA.tau);
        end
            
            
        currentFigure = figure('Visible', 'off');
        
            x = 1:length(rq0_me.factorA.label);
            
            %--------------------------------------------------------------
            % system main effects on whole corpus            
            data.mean = rq0_me.factorA.mean.';
            data.ciLow = rq0_me.factorA.ci(:, 1).';
            data.ciHigh = rq0_me.factorA.ci(:, 2).';        

            plot(x, data.mean, 'Color', rgb('Black'), 'LineWidth', 3, 'LineStyle', '--');

            hold on
            
            hFill = fill([x fliplr(x)],[data.ciHigh fliplr(data.ciLow)], rgb('Gray'), ...
                        'LineStyle', 'none', 'EdgeAlpha', ALPHA, 'FaceAlpha', ALPHA);

            % send the fill to back
            uistack(hFill, 'bottom');

            % Exclude fill from legend
            set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
            
            %--------------------------------------------------------------
            % system main effects on shards   
            lbl = cell(1, length(balanced));
            
            for i = 1:length(balanced)                
                tmp = rq1_me{i};
                
                data.mean = tmp.factorA.mean.';
                data.ciLow = tmp.factorA.ci(:, 1).';
                data.ciHigh = tmp.factorA.ci(:, 2).';
                
                plot(x, data.mean, 'Color', EXPERIMENT.analysis.balanced.(balanced{i}).color, 'LineWidth', 2.5);
                
                hFill = fill([x fliplr(x)],[data.ciHigh fliplr(data.ciLow)], EXPERIMENT.analysis.balanced.(balanced{i}).color, ...
                    'LineStyle', 'none', 'EdgeAlpha', ALPHA, 'FaceAlpha', ALPHA);
                
                % send the fill to back
                uistack(hFill, 'bottom');
                
                % Exclude fill from legend
                set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
                
%                 lbl{i} = sprintf('%s ($\\tau$ = %5.4f; blc value = %5.4f)',  ...
%                     balanced{i}, rq1_me{i}.factorA.tau, rq1_blc{i}.value);

                lbl{i} = sprintf('%s (balancing value = %5.4f)',  ...
                    balanced{i}, rq1_blc{i}.value);
            end
                 
            legend([{'Whole corpus'} lbl], 'Interpreter', 'latex');
            
            ax = gca;
            ax.FontSize = 24;
            ax.TickLabelInterpreter = 'latex';

            ax.XTick = [];
            ax.XTickLabel = [];
            %ax.XTickLabelRotation = 90;

            ax.XLabel.Interpreter = 'latex';
            ax.XLabel.String = sprintf('\\texttt{%s} systems using \\texttt{%s} shards', strrep(trackID, '_', '\_'), strrep(EXPERIMENT.split.(splitID).label, '_', '\_'));

            ax.YLabel.Interpreter = 'latex';
            ax.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
            
            ax.XLim = [0 length(x)];
            ax.YLim = [0 ax.YLim(2)];

            title(sprintf('Correlation between rankings of systems on whole collection and shards $\\tau$ = %5.4f', rq1_me{1}.factorA.tau), 'FontSize', 24, 'Interpreter', 'latex');            
            
            % title(sprintf('Correlation between rankings of systems on whole collection and shards $\\tau$ = %5.4f', ), 'FontSize', 24, 'Interpreter', 'latex');            
            
            
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 17];
        currentFigure.PaperPosition = [1 1 40 15];

        figureID = EXPERIMENT.pattern.identifier.figure.me(join(string(balanced), "_"), sstype, quartile, splitID, trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
    
    end % measure
           
    fprintf('\n\n######## Total elapsed time for plotting main effects on track %s (%s): %s ########\n\n', ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
end

