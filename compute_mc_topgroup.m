%% compute_mc_topgroup
% 
% Computes the top group according to multiple comparison tests for the 
% ANOVA analyses.
%
%% Synopsis
%
%   [tg] = compute_mc_topgroup(mc, me)
%  
% *Parameters*
%
% * *|mc|* - the output of the |multcompare| function, containing all the
% pairwise comparisons.
% * *|me|* - the main effects, i.e. a struct where |me.factorA.label|
% contains the identifiers of the systems and |me.factorA.mean| contains 
% the mean performance for each system.
%
%
% *Returns*
%
% * *|tg|* - a cell array containing the identifiers of the systems in the
% top group.


%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [tg] = compute_mc_topgroup(mc, me)

    common_parameters

    % the index of the top systems
    topSys = find(me.factorA.mean == max(me.factorA.mean));
    
    % pick just the first one - the others will be in the top group anyway
    topSys = topSys(1);
    
    % find all the systems not significantly different from topSys
    idx = (mc(:, 1) == topSys | mc(:, 2) == topSys) & mc(:, 6) > EXPERIMENT.analysis.alpha.threshold;
    
    % if there is no system not significantly different from topSys, then
    % the top group is constituted by topSys alone
    if(~any(idx))
        tg = me.factorA.label(topSys);
        return
    end
    
    % find the indexes of the systems in the top group and remove
    % duplicates
    tmp = mc(idx, 1:2);
    tmp = unique(tmp(:));
    
    % return the identifiers of the systems in the top group
    tg = me.factorA.label(tmp);
end
