%% compute_md1_analysis
% 
% Computes the Topic/System effects ANOVA on the whole corpus.

%% Synopsis
%
%   [] = compute_md1_analysis(trackID, sstype, quartile, startMeasure, endMeasure, threads)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use q1 for top
% quartile; q2 for median; q3 for up to third quartile; q4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
% * *|threads|* - the maximum number of threads to be used. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = compute_md1_analysis(trackID, sstype, quartile, startMeasure, endMeasure, threads)

    persistent TAG;
    
    if isempty(TAG)
        TAG = 'md1';      
    end

    % check the number of input parameters
    narginchk(3, 6);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';
    
    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');
                 
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');
        
                
    % check that quartile is a non-empty cell array
    validateattributes(quartile, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'quartile');
        
    if iscell(quartile)
        % check that quartile is a cell array of strings with one element
        assert(iscellstr(quartile) && numel(quartile) == 1, ...
            'MATTERS:IllegalArgument', 'Expected quartile to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    quartile = char(strtrim(quartile));
    quartile = quartile(:).';
    
    % check that quartile assumes a valid value
    validatestring(quartile, ...
        EXPERIMENT.analysis.quartile.list, '', 'quartile');
        
    if nargin >= 5
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');
        
        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');        
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end
    
    if nargin == 6
        % the number of threads must be at maximum equal to the number of
        % physical cores
        validateattributes(threads, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', feature('numcores') }, '', 'threads');
        
        maxNumCompThreads(threads);
    else
        threads = maxNumCompThreads('automatic');        
    end
    
    % start of overall computations
    startComputation = tic;
    
    % we always assume a balanced design on the whole corpus and check for
    % the absence of NaN!
    balanced = 'b';
    
    % the type of balancing performed
    blc.type = balanced;
    % since we do not force balancing, because we assume it, the balancing
    % value is NaN
    blc.value = NaN;      
                  
    fprintf('\n\n######## Performing %s ANOVA analysis on track %s (%s) ########\n\n', ...
        TAG, EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper);
    
    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * %s\n', TAG);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).name);
    fprintf('    * %s\n', EXPERIMENT.analysis.(TAG).description);
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %s - %s \n', quartile, EXPERIMENT.analysis.quartile.(quartile).description);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));
    fprintf('  - threads: %d\n\n', threads);
              
    % for each measure
    for m = startMeasure:endMeasure
         
        start = tic;
        
        fprintf('\n+ Analysing %s\n', EXPERIMENT.measure.getAcronym(m));
        
        fprintf('  - loading the data\n');
        
        mid = EXPERIMENT.measure.list{m};
                        
        % load the whole corpus measure
        measureID = EXPERIMENT.pattern.identifier.measure(mid, EXPERIMENT.track.(trackID).corpus, trackID);
        
        serload2(EXPERIMENT.pattern.file.measure.corpus(trackID, measureID), ...
            'WorkspaceVarNames', {'measure'}, ...
            'FileVarNames', {measureID});
        
        % determine the indexes of the systems in the requested quartile of
        % the measure with respect to the whole corpus
        [idx, mm] = compute_quartile(quartile, measure);
        
        % reorder systems by descending mean of the measure on the
        % whole corpus and keep only those in the requested quartile
        measure = measure(:, idx);
        
        % the number of topics and runs
        T = height(measure);
        R = width(measure);
        
        % total number of elements in the list
        N = T * R;
        
        data = measure{:, :}(:);
        subject = repmat(measure.Properties.RowNames, R, 1);
        factorA = repmat(measure.Properties.VariableNames, T, 1);
        factorA = factorA(:);
        
        clear measure;
                
        fprintf('  - analysing the data\n');
                
        [~, tbl, sts] = EXPERIMENT.analysis.(TAG).compute(data, subject, ...
            factorA, sstype);
        
        df_subject = tbl{2,3};
        ss_subject = tbl{2,2};
        F_subject = tbl{2,6};
        
        df_factorA = tbl{3,3};
        ss_factorA = tbl{3,2};
        F_factorA = tbl{3,6};
                
        df_error = tbl{4,3};
        ss_error = tbl{4, 2};
        ms_error = tbl{4, 5};
        
        % compute the strength of association and the power      
        soa.omega2p.subject = df_subject * (F_subject - 1) / (df_subject * (F_subject - 1) + N);
        soa.omega2p.factorA = df_factorA * (F_factorA - 1) / (df_factorA * (F_factorA - 1) + N);
        
        soa.eta2p.subject = ss_subject / (ss_subject + ss_error);
        soa.eta2p.factorA = ss_factorA / (ss_factorA + ss_error);
        
        soa.f2.subject = ss_subject / ss_error;
        soa.f2.factorA = ss_factorA / ss_error;
        
        pwr.subject.Fc = finv(1 - EXPERIMENT.analysis.alpha.threshold, df_subject, df_error);
        pwr.subject.lambda = soa.f2.subject * N;
        pwr.subject.power = ncfcdf(pwr.subject.Fc, df_subject, df_error, pwr.subject.lambda, 'upper');
        
        pwr.factorA.Fc = finv(1 - EXPERIMENT.analysis.alpha.threshold, df_factorA, df_error);
        pwr.factorA.lambda = soa.f2.factorA * N;
        pwr.factorA.power = ncfcdf(pwr.factorA.Fc, df_factorA, df_error, pwr.factorA.lambda, 'upper');

        % main effects
        me.factorA.label = unique(factorA, 'stable');
        [me.factorA.mean, me.factorA.std] = grpstats(data(:), factorA(:), {'mean', 'std'});
               
        assert(issorted(me.factorA.mean, 'descend'), ...
            'MATTERS:IllegalState', 'Expected main effect of factorA (%s) to be sorted in descending order.', EXPERIMENT.analysis.label.factorA);
        
        % this is just to double check that mm is the marginal mean of the
        % system factor since it will be used in the other MDs to compute 
        % the correlation wrt the whole corpus
        assert(isequal(mm(:), me.factorA.mean(:)), ...
            'MATTERS:IllegalState', 'Expected main effect of factorA (%s) to be equal to the mean of the measure.', EXPERIMENT.analysis.label.factorA);
        
        
        % compute the Student's t CI
        me.factorA.student.t = tinv(1 - EXPERIMENT.analysis.alpha.threshold/2, T - 1);
        me.factorA.student.halfWidth = me.factorA.student.t .* me.factorA.std / sqrt(T);
        me.factorA.student.ci = [me.factorA.mean - me.factorA.student.halfWidth, me.factorA.mean + me.factorA.student.halfWidth];
        
        % compute the ANOVA CI
        me.factorA.anova.t = tinv(1 - EXPERIMENT.analysis.alpha.threshold/2, df_error);
        me.factorA.anova.halfWidth = me.factorA.student.t .* sqrt(ms_error/T);
        me.factorA.anova.ci = [me.factorA.mean - me.factorA.anova.halfWidth, me.factorA.mean + me.factorA.anova.halfWidth];
                
        % compute the Tukey CI
        me.factorA.tukey.q = internal.stats.stdrinv(1 - EXPERIMENT.analysis.alpha.threshold, df_error, R);
        me.factorA.tukey.halfWidth = me.factorA.tukey.q * sqrt(ms_error/T) / 2;
        me.factorA.tukey.ci = [me.factorA.mean - me.factorA.tukey.halfWidth, me.factorA.mean + me.factorA.tukey.halfWidth];
                               
        % multiple comparison of systems
        mc = EXPERIMENT.analysis.multcompare.system(sts);

        % compute the top group
        me.factorA.tg = compute_mc_topgroup(mc, me);
        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(TAG, balanced, sstype, quartile, mid, EXPERIMENT.track.(trackID).corpus, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(TAG, balanced, sstype, quartile, mid, EXPERIMENT.track.(trackID).corpus, trackID);
        anovaTableID = EXPERIMENT.pattern.identifier.anova.tbl(TAG, balanced, sstype, quartile, mid, EXPERIMENT.track.(trackID).corpus, trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(TAG, balanced, sstype, quartile, mid, EXPERIMENT.track.(trackID).corpus, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(TAG, balanced, sstype, quartile, mid, EXPERIMENT.track.(trackID).corpus, trackID);
        anovaPwrID = EXPERIMENT.pattern.identifier.anova.pwr(TAG, balanced, sstype, quartile, mid, EXPERIMENT.track.(trackID).corpus, trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(TAG, balanced, sstype, quartile, mid, EXPERIMENT.track.(trackID).corpus, trackID);
        
        sersave2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
                'WorkspaceVarNames', {'me', 'tbl', 'blc', 'soa', 'pwr' 'mc'}, ...
                'FileVarNames', {anovaMeID, anovaTableID, anovaBlcID, anovaSoAID, anovaPwrID, anovaMcID});
                
        clear idx mm data me tbl sts soa pwr mc;
        
        fprintf('  - elapsed time: %s\n', elapsedToHourMinutesSeconds(toc(start)));
                     
    end % for measure
                   
    fprintf('\n\n######## Total elapsed time for performing %s ANOVA analysis on track %s (%s): %s ########\n\n', ...
           TAG, ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
end

