%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration

% if we are running on the cluster 
if (strcmpi(computer, 'GLNXA64'))    
    addpath(genpath('/nas1/promise/ims/ferro/matters/'))   % ave/eva
    addpath(genpath('/ssd/data/ferro/matters/'))   % grace
end

% The base path
if (strcmpi(computer, 'GLNXA64')) % if we are running on the cluster    
    % EXPERIMENT.path.base = '/nas1/promise/ims/ferro/SIGIR2019-FF/experiment/'; % ave/eva
   EXPERIMENT.path.base = '/ssd/data/ferro/SIGIR2019-FS/experiment/';  % grace
else % if we are running on the local machine
    EXPERIMENT.path.base = '/Users/ferro/Documents/pubblicazioni/2019/SIGIR2019/FS/experiment/';  
end

% The path for the corpora, i.e. text files containing the list of
% documents for a corpus
EXPERIMENT.path.corpus = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'corpus', filesep);

% The path for the shards, i.e. a directories containing text files, 
% each one listing documents from a corpus according to some criterion
EXPERIMENT.path.shard = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'shard', filesep);

% The path for the datasets, i.e. the runs and the pools of both original
% tracks and sub-corpora
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);

% The path for analyses
EXPERIMENT.path.analysis = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'analysis', filesep);

% The path for figures
EXPERIMENT.path.figure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'figure', filesep);

% The path for reports
EXPERIMENT.path.report = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'report', filesep);

%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'SIGIR 2019 FS';


%% Configuration for Corpora

EXPERIMENT.corpus.list = {'GOV2', 'NYT', 'TIP', 'WAPO', 'WT10g'};
EXPERIMENT.corpus.number = length(EXPERIMENT.corpus.list);

% The full TIPSTER corpus
EXPERIMENT.corpus.TIP.id = 'TIP';
EXPERIMENT.corpus.TIP.name = 'TIPSTER, Disk 4-5 minus Congressional Record';
EXPERIMENT.corpus.TIP.size = 528155;

% The full WT10g corpus
EXPERIMENT.corpus.WT10g.id = 'WT10g';
EXPERIMENT.corpus.WT10g.name = 'WT10g, a crawl of Web preserving some desiderablde corpus properties (around 1999-2000)';
EXPERIMENT.corpus.WT10g.size = 1692096;

% The full GOV2 corpus
EXPERIMENT.corpus.GOV2.id = 'GOV2';
EXPERIMENT.corpus.GOV2.name = 'A crawl of .gov sites (early 2004)';
EXPERIMENT.corpus.GOV2.size = 25205179;

% The full NYT corpus
EXPERIMENT.corpus.NYT.id = 'NYT';
EXPERIMENT.corpus.NYT.name = 'The New York Times Annotated Corpus contains articles published between January 1, 1987 and June 19, 2007';
EXPERIMENT.corpus.NYT.size = 1855658;

% The full WAPO corpus
EXPERIMENT.corpus.WAPO.id = 'WAPO';
EXPERIMENT.corpus.WAPO.name = 'The TREC Washington Post Corpus contains news articles and blog posts from January 2012 through August 2017';
EXPERIMENT.corpus.WAPO.size = 1855658;


%% Configuration for Splits

%EXPERIMENT.split.list = {'WT10g_ALL_TLD_EVEN_SIZE', 'WT10g_ALL_TLD', 'WT10g_HALF_TLD', 'WT10g_QUARTER_TLD', 'WT10g_EIGHTH_TLD', ...
%    'WT10g_ALL_TLD_RANDOM_SIZE', 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE', ...
%    'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25', 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10','WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5', 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2',  ...    
%    'TIPSTER_DS'};
EXPERIMENT.split.list = {'WT10g_RNDE_02', 'WT10g_RNDE_03', 'WT10g_RNDE_04', 'WT10g_RNDE_05', 'WT10g_RNDE_10', 'WT10g_RNDE_25', 'WT10g_RNDE_50', ...
                         'TIP_RNDE_02', 'TIP_RNDE_03', 'TIP_RNDE_04', 'TIP_RNDE_05', 'TIP_RNDE_10', 'TIP_RNDE_25', 'TIP_RNDE_50', ...
                         'WAPO_RNDE_02', 'WAPO_RNDE_03', 'WAPO_RNDE_04', 'WAPO_RNDE_05', 'WAPO_RNDE_10', 'WAPO_RNDE_25', 'WAPO_RNDE_50'};
EXPERIMENT.split.number = length(EXPERIMENT.split.list);

% The total number of sample for each split
EXPERIMENT.split.sample = 10;

EXPERIMENT.split.WT10g_RNDE_02.id = 'WT10g_RNDE_02';
EXPERIMENT.split.WT10g_RNDE_02.name = 'WT10g divided in 2 even size shards';
EXPERIMENT.split.WT10g_RNDE_02.corpus = 'WT10g';
EXPERIMENT.split.WT10g_RNDE_02.shard = 2;
EXPERIMENT.split.WT10g_RNDE_02.ratio = repmat(1/EXPERIMENT.split.WT10g_RNDE_02.shard, 1, EXPERIMENT.split.WT10g_RNDE_02.shard);

EXPERIMENT.split.WT10g_RNDE_03.id = 'WT10g_RNDE_03';
EXPERIMENT.split.WT10g_RNDE_03.name = 'WT10g divided in 3 even size shards';
EXPERIMENT.split.WT10g_RNDE_03.corpus = 'WT10g';
EXPERIMENT.split.WT10g_RNDE_03.shard = 3;
EXPERIMENT.split.WT10g_RNDE_03.ratio = repmat(1/EXPERIMENT.split.WT10g_RNDE_03.shard, 1, EXPERIMENT.split.WT10g_RNDE_03.shard);

EXPERIMENT.split.WT10g_RNDE_04.id = 'WT10g_RNDE_04';
EXPERIMENT.split.WT10g_RNDE_04.name = 'WT10g divided in 4 even size shards';
EXPERIMENT.split.WT10g_RNDE_04.corpus = 'WT10g';
EXPERIMENT.split.WT10g_RNDE_04.shard = 4;
EXPERIMENT.split.WT10g_RNDE_04.ratio = repmat(1/EXPERIMENT.split.WT10g_RNDE_04.shard, 1, EXPERIMENT.split.WT10g_RNDE_04.shard);

EXPERIMENT.split.WT10g_RNDE_05.id = 'WT10g_RNDE_05';
EXPERIMENT.split.WT10g_RNDE_05.name = 'WT10g divided in 5 even size shards';
EXPERIMENT.split.WT10g_RNDE_05.corpus = 'WT10g';
EXPERIMENT.split.WT10g_RNDE_05.shard = 5;
EXPERIMENT.split.WT10g_RNDE_05.ratio = repmat(1/EXPERIMENT.split.WT10g_RNDE_05.shard, 1, EXPERIMENT.split.WT10g_RNDE_05.shard);

EXPERIMENT.split.WT10g_RNDE_10.id = 'WT10g_RNDE_10';
EXPERIMENT.split.WT10g_RNDE_10.name = 'WT10g divided in 10 even size shards';
EXPERIMENT.split.WT10g_RNDE_10.corpus = 'WT10g';
EXPERIMENT.split.WT10g_RNDE_10.shard = 10;
EXPERIMENT.split.WT10g_RNDE_10.ratio = repmat(1/EXPERIMENT.split.WT10g_RNDE_10.shard, 1, EXPERIMENT.split.WT10g_RNDE_10.shard);

EXPERIMENT.split.WT10g_RNDE_25.id = 'WT10g_RNDE_25';
EXPERIMENT.split.WT10g_RNDE_25.name = 'WT10g divided in 25 even size shards';
EXPERIMENT.split.WT10g_RNDE_25.corpus = 'WT10g';
EXPERIMENT.split.WT10g_RNDE_25.shard = 25;
EXPERIMENT.split.WT10g_RNDE_25.ratio = repmat(1/EXPERIMENT.split.WT10g_RNDE_25.shard, 1, EXPERIMENT.split.WT10g_RNDE_25.shard);

EXPERIMENT.split.WT10g_RNDE_50.id = 'WT10g_RNDE_50';
EXPERIMENT.split.WT10g_RNDE_50.name = 'WT10g divided in 50 even size shards';
EXPERIMENT.split.WT10g_RNDE_50.corpus = 'WT10g';
EXPERIMENT.split.WT10g_RNDE_50.shard = 50;
EXPERIMENT.split.WT10g_RNDE_50.ratio = repmat(1/EXPERIMENT.split.WT10g_RNDE_50.shard, 1, EXPERIMENT.split.WT10g_RNDE_50.shard);

EXPERIMENT.split.TIP_RNDE_02.id = 'TIP_RNDE_02';
EXPERIMENT.split.TIP_RNDE_02.name = 'TIPSTER divided in 2 even size shards';
EXPERIMENT.split.TIP_RNDE_02.corpus = 'TIP';
EXPERIMENT.split.TIP_RNDE_02.shard = 2;
EXPERIMENT.split.TIP_RNDE_02.ratio = repmat(1/EXPERIMENT.split.TIP_RNDE_02.shard, 1, EXPERIMENT.split.TIP_RNDE_02.shard);

EXPERIMENT.split.TIP_RNDE_03.id = 'TIP_RNDE_03';
EXPERIMENT.split.TIP_RNDE_03.name = 'TIPSTER divided in 3 even size shards';
EXPERIMENT.split.TIP_RNDE_03.corpus = 'TIP';
EXPERIMENT.split.TIP_RNDE_03.shard = 3;
EXPERIMENT.split.TIP_RNDE_03.ratio = repmat(1/EXPERIMENT.split.TIP_RNDE_03.shard, 1, EXPERIMENT.split.TIP_RNDE_03.shard);

EXPERIMENT.split.TIP_RNDE_04.id = 'TIP_RNDE_04';
EXPERIMENT.split.TIP_RNDE_04.name = 'TIPSTER divided in 4 even size shards';
EXPERIMENT.split.TIP_RNDE_04.corpus = 'TIP';
EXPERIMENT.split.TIP_RNDE_04.shard = 4;
EXPERIMENT.split.TIP_RNDE_04.ratio = repmat(1/EXPERIMENT.split.TIP_RNDE_04.shard, 1, EXPERIMENT.split.TIP_RNDE_04.shard);

EXPERIMENT.split.TIP_RNDE_05.id = 'TIP_RNDE_05';
EXPERIMENT.split.TIP_RNDE_05.name = 'TIPSTER divided in 5 even size shards';
EXPERIMENT.split.TIP_RNDE_05.corpus = 'TIP';
EXPERIMENT.split.TIP_RNDE_05.shard = 5;
EXPERIMENT.split.TIP_RNDE_05.ratio = repmat(1/EXPERIMENT.split.TIP_RNDE_05.shard, 1, EXPERIMENT.split.TIP_RNDE_05.shard);

EXPERIMENT.split.TIP_RNDE_10.id = 'TIP_RNDE_10';
EXPERIMENT.split.TIP_RNDE_10.name = 'TIPSTER divided in 10 even size shards';
EXPERIMENT.split.TIP_RNDE_10.corpus = 'TIP';
EXPERIMENT.split.TIP_RNDE_10.shard = 10;
EXPERIMENT.split.TIP_RNDE_10.ratio = repmat(1/EXPERIMENT.split.TIP_RNDE_10.shard, 1, EXPERIMENT.split.TIP_RNDE_10.shard);

EXPERIMENT.split.TIP_RNDE_25.id = 'TIP_RNDE_25';
EXPERIMENT.split.TIP_RNDE_25.name = 'TIPSTER divided in 25 even size shards';
EXPERIMENT.split.TIP_RNDE_25.corpus = 'TIP';
EXPERIMENT.split.TIP_RNDE_25.shard = 25;
EXPERIMENT.split.TIP_RNDE_25.ratio = repmat(1/EXPERIMENT.split.TIP_RNDE_25.shard, 1, EXPERIMENT.split.TIP_RNDE_25.shard);

EXPERIMENT.split.TIP_RNDE_50.id = 'TIP_RNDE_50';
EXPERIMENT.split.TIP_RNDE_50.name = 'TIPSTER divided in 50 even size shards';
EXPERIMENT.split.TIP_RNDE_50.corpus = 'TIP';
EXPERIMENT.split.TIP_RNDE_50.shard = 50;
EXPERIMENT.split.TIP_RNDE_50.ratio = repmat(1/EXPERIMENT.split.TIP_RNDE_50.shard, 1, EXPERIMENT.split.TIP_RNDE_50.shard);

EXPERIMENT.split.WAPO_RNDE_02.id = 'WAPO_RNDE_02';
EXPERIMENT.split.WAPO_RNDE_02.name = 'WAPO divided in 2 even size shards';
EXPERIMENT.split.WAPO_RNDE_02.corpus = 'WAPO';
EXPERIMENT.split.WAPO_RNDE_02.shard = 2;
EXPERIMENT.split.WAPO_RNDE_02.ratio = repmat(1/EXPERIMENT.split.WAPO_RNDE_02.shard, 1, EXPERIMENT.split.WAPO_RNDE_02.shard);

EXPERIMENT.split.WAPO_RNDE_03.id = 'WAPO_RNDE_03';
EXPERIMENT.split.WAPO_RNDE_03.name = 'WAPO divided in 3 even size shards';
EXPERIMENT.split.WAPO_RNDE_03.corpus = 'WAPO';
EXPERIMENT.split.WAPO_RNDE_03.shard = 3;
EXPERIMENT.split.WAPO_RNDE_03.ratio = repmat(1/EXPERIMENT.split.WAPO_RNDE_03.shard, 1, EXPERIMENT.split.WAPO_RNDE_03.shard);

EXPERIMENT.split.WAPO_RNDE_04.id = 'WAPO_RNDE_04';
EXPERIMENT.split.WAPO_RNDE_04.name = 'WAPO divided in 4 even size shards';
EXPERIMENT.split.WAPO_RNDE_04.corpus = 'WAPO';
EXPERIMENT.split.WAPO_RNDE_04.shard = 4;
EXPERIMENT.split.WAPO_RNDE_04.ratio = repmat(1/EXPERIMENT.split.WAPO_RNDE_04.shard, 1, EXPERIMENT.split.WAPO_RNDE_04.shard);

EXPERIMENT.split.WAPO_RNDE_05.id = 'WAPO_RNDE_05';
EXPERIMENT.split.WAPO_RNDE_05.name = 'WAPO divided in 5 even size shards';
EXPERIMENT.split.WAPO_RNDE_05.corpus = 'WAPO';
EXPERIMENT.split.WAPO_RNDE_05.shard = 5;
EXPERIMENT.split.WAPO_RNDE_05.ratio = repmat(1/EXPERIMENT.split.WAPO_RNDE_05.shard, 1, EXPERIMENT.split.WAPO_RNDE_05.shard);

EXPERIMENT.split.WAPO_RNDE_10.id = 'WAPO_RNDE_10';
EXPERIMENT.split.WAPO_RNDE_10.name = 'WAPO divided in 10 even size shards';
EXPERIMENT.split.WAPO_RNDE_10.corpus = 'WAPO';
EXPERIMENT.split.WAPO_RNDE_10.shard = 10;
EXPERIMENT.split.WAPO_RNDE_10.ratio = repmat(1/EXPERIMENT.split.WAPO_RNDE_10.shard, 1, EXPERIMENT.split.WAPO_RNDE_10.shard);

EXPERIMENT.split.WAPO_RNDE_25.id = 'WAPO_RNDE_25';
EXPERIMENT.split.WAPO_RNDE_25.name = 'WAPO divided in 25 even size shards';
EXPERIMENT.split.WAPO_RNDE_25.corpus = 'WAPO';
EXPERIMENT.split.WAPO_RNDE_25.shard = 25;
EXPERIMENT.split.WAPO_RNDE_25.ratio = repmat(1/EXPERIMENT.split.WAPO_RNDE_25.shard, 1, EXPERIMENT.split.WAPO_RNDE_25.shard);

EXPERIMENT.split.WAPO_RNDE_50.id = 'WAPO_RNDE_50';
EXPERIMENT.split.WAPO_RNDE_50.name = 'WAPO divided in 50 even size shards';
EXPERIMENT.split.WAPO_RNDE_50.corpus = 'WAPO';
EXPERIMENT.split.WAPO_RNDE_50.shard = 50;
EXPERIMENT.split.WAPO_RNDE_50.ratio = repmat(1/EXPERIMENT.split.WAPO_RNDE_50.shard, 1, EXPERIMENT.split.WAPO_RNDE_50.shard);



%---------------------

% WT10g divided in 59 even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.id = 'WT10g_ALL_TLD_EVEN_SIZE';
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.name =  'WT10g divided in 59 even size shards, corresponding to all the Top Level Domains';
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.shard = 59;
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_EVEN_SIZE.label = 'WT10g_TLDE_59';

% WT10g divided in 59 TLD shards
EXPERIMENT.split.WT10g_ALL_TLD.id = 'WT10g_ALL_TLD';
EXPERIMENT.split.WT10g_ALL_TLD.name =  'WT10g divided in 59 uneven size shards, corresponding to the all Top Level Domains';
EXPERIMENT.split.WT10g_ALL_TLD.shard = 59;
EXPERIMENT.split.WT10g_ALL_TLD.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD.label = 'WT10g_TLDU_59';

% WT10g divided in 30 TLD shards
EXPERIMENT.split.WT10g_HALF_TLD.id = 'WT10g_HALF_TLD';
EXPERIMENT.split.WT10g_HALF_TLD.name =  'WT10g divided in 30 uneven size shards, corresponding to half groupings of the Top Level Domains';
EXPERIMENT.split.WT10g_HALF_TLD.shard = 30;
EXPERIMENT.split.WT10g_HALF_TLD.corpus = 'WT10g';
EXPERIMENT.split.WT10g_HALF_TLD.label = 'WT10g_TLDU_30';

% WT10g divided in 15 TLD shards
EXPERIMENT.split.WT10g_QUARTER_TLD.id = 'WT10g_QUARTER_TLD';
EXPERIMENT.split.WT10g_QUARTER_TLD.name =  'WT10g divided in 15 uneven size shards, corresponding to quarter groupings of the Top Level Domains';
EXPERIMENT.split.WT10g_QUARTER_TLD.shard = 15;
EXPERIMENT.split.WT10g_QUARTER_TLD.corpus = 'WT10g';
EXPERIMENT.split.WT10g_QUARTER_TLD.label = 'WT10g_TLDU_15';

% WT10g divided in 8 TLD shards
EXPERIMENT.split.WT10g_EIGHTH_TLD.id = 'WT10g_EIGHTH_TLD';
EXPERIMENT.split.WT10g_EIGHTH_TLD.name =  'WT10g divided in 8 uneven size shards, corresponding to eighth groupings of the Top Level Domains';
EXPERIMENT.split.WT10g_EIGHTH_TLD.shard = 8;
EXPERIMENT.split.WT10g_EIGHTH_TLD.corpus = 'WT10g';
EXPERIMENT.split.WT10g_EIGHTH_TLD.label = 'WT10g_TLDU_8';

% WT10g divided in 59 random size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.id = 'WT10g_ALL_TLD_RANDOM_SIZE';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.name =  'WT10g divided in 59 random unven size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.shard = 59;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_SIZE.label = 'WT10g_RNDU_59';

% WT10g divided in 59 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.name =  'WT10g divided in 59 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.shard = 59;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE.label = 'WT10g_RNDE_59';

% WT25g divided in 25 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.name =  'WT10g divided in 25 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.shard = 25;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.label = 'WT10g_RNDE_25';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_25.color = rgb('Goldenrod');

% WT10g divided in 10 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.name =  'WT10g divided in 10 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.shard = 10;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.label = 'WT10g_RNDE_10';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_10.color = rgb('ForestGreen');

% WT10g divided in 5 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.name =  'WT10g divided in 5 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.shard = 5;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.label = 'WT10g_RNDE_5';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_5.color = rgb('RoyalBlue');

% WT10g divided in 2 random even size TLD shards
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.id = 'WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.name =  'WT10g divided in 2 random even size shards';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.shard = 2;
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.corpus = 'WT10g';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.label = 'WT10g_RNDE_2';
EXPERIMENT.split.WT10g_ALL_TLD_RANDOM_EVEN_SIZE_2.color = rgb('FireBrick');


% TIPSTER divided in 4 shards by document source
EXPERIMENT.split.TIPSTER_DS.id = 'TIPSTER_DS';
EXPERIMENT.split.TIPSTER_DS.name =  'TIPSTER divided by document source';
EXPERIMENT.split.TIPSTER_DS.shard = 4;
EXPERIMENT.split.TIPSTER_DS.shardLabels = {'TIPFBIS', 'TIPFR', 'TIPFT', 'TIPLA'};
EXPERIMENT.split.TIPSTER_DS.shardNames = {'TIPSTER, Foreign Broadcast Information Service', 'TIPSTER, Financial Register', 'TIPSTER, Financial Times', 'TIPSTER, Los Angeles Times'};
EXPERIMENT.split.TIPSTER_DS.corpus = 'TIPSTER';
EXPERIMENT.split.TIPSTER_DS.label = 'TIPSTER_DS';

% Returns the label of a shard in its split, given its index
EXPERIMENT.split.getShardLabel = @(splitID, idx) ( EXPERIMENT.split.(splitID).shardLabels{idx} ); 

% Returns the name of a shard in its split, given its index
EXPERIMENT.split.getShardName = @(splitID, idx) ( EXPERIMENT.split.(splitID).shardNames{idx} ); 

%% Configuration for Tracks

EXPERIMENT.track.list = {'T08', 'T09', 'T27'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);


% TREC 08, 1999, Adhoc
EXPERIMENT.track.T08.id = 'T08';
EXPERIMENT.track.T08.name = 'TREC 08, 1999, Adhoc';
EXPERIMENT.track.T08.corpus = 'TIP';
EXPERIMENT.track.T08.topics = 50;
EXPERIMENT.track.T08.runs = 129;
EXPERIMENT.track.T08.runLength = 1000;
EXPERIMENT.track.T08.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08.pool.delimiter = 'space';
EXPERIMENT.track.T08.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/runs/all';
EXPERIMENT.track.T08.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08.run.singlePrecision = true;
EXPERIMENT.track.T08.run.delimiter = 'tab';
EXPERIMENT.track.T08.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08.run.delimiter, 'Verbose', false);

% TREC 09, 2000, Web
EXPERIMENT.track.T09.id = 'T09';
EXPERIMENT.track.T09.name = 'TREC 09, 2000, Web';
EXPERIMENT.track.T09.corpus = 'WT10g';
EXPERIMENT.track.T09.topics = 50;
EXPERIMENT.track.T09.runs = 104;
EXPERIMENT.track.T09.runLength = 1000;
EXPERIMENT.track.T09.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_09_2000_Web/pool/qrels.trec9.main_web.txt';
EXPERIMENT.track.T09.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T09.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T09.pool.delimiter = 'space';
EXPERIMENT.track.T09.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T09.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T09.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T09.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T09.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_09_2000_Web/runs/all';
EXPERIMENT.track.T09.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T09.run.singlePrecision = true;
EXPERIMENT.track.T09.run.delimiter = 'tab';
EXPERIMENT.track.T09.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T09.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T09.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T09.run.delimiter, 'Verbose', false);

% TREC 27, 2018, Common Core
EXPERIMENT.track.T27.id = 'T27';
EXPERIMENT.track.T27.name =  'TREC 27, 2018, Common Core';
EXPERIMENT.track.T27.corpus = 'WAPO';
EXPERIMENT.track.T27.topics = 50;
EXPERIMENT.track.T27.runs = 72;
EXPERIMENT.track.T27.runLength = 10000;
EXPERIMENT.track.T27.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_27_2018_Core/pool/qrels.trec27.core.txt';
EXPERIMENT.track.T27.pool.relevanceDegrees = {'NotRelevant', 'Relevant', 'HighlyRelevant'};
EXPERIMENT.track.T27.pool.relevanceGrades = 0:2;
EXPERIMENT.track.T27.pool.delimiter = 'space';
EXPERIMENT.track.T27.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T27.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T27.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T27.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T27.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_27_2018_Core/runs';
EXPERIMENT.track.T27.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T27.run.singlePrecision = true;
EXPERIMENT.track.T27.run.delimiter = 'tab';
EXPERIMENT.track.T27.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T27.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T27.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T27.run.delimiter, 'Verbose', false);

% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 


%% Patterns for file names

% TXT - Pattern EXPERIMENT.path.base/corpus/<corpusID>.txt
EXPERIMENT.pattern.file.corpus = @(corpusID) sprintf('%1$s%2$s.txt', EXPERIMENT.path.corpus, corpusID);

% TXT - Pattern EXPERIMENT.path.shard/<splitID>/<shardID>.txt
EXPERIMENT.pattern.file.shard = @(splitID, shardID) sprintf('%1$s%2$s%3$s%2$s%4$s.txt', EXPERIMENT.path.shard, filesep, splitID, shardID);

% ALL - Pattern <path>/<trackID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general.track = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);

% ALL - Pattern <path>/<trackID>/<splitID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general.shard = @(path, trackID, splitID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s%3$s%5$s.%6$s', path, trackID, filesep, splitID, fileID, ext);

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset.corpus = @(trackID, datasetID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.dataset, trackID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<splitID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset.shard = @(trackID, splitID, datasetID) EXPERIMENT.pattern.file.general.shard(EXPERIMENT.path.dataset, trackID, splitID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure.corpus = @(trackID, measureID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.measure, trackID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<splitID>/<measureID>.mat
EXPERIMENT.pattern.file.measure.shard = @(trackID, splitID, measureID) EXPERIMENT.pattern.file.general.shard(EXPERIMENT.path.measure, trackID, splitID, measureID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis.corpus = @(trackID, analysisID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.analysis, trackID, analysisID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/analysis/<trackID>/<splitID>/<analysisID>.mat
EXPERIMENT.pattern.file.analysis.shard = @(trackID, splitID, analysisID) EXPERIMENT.pattern.file.general.shard(EXPERIMENT.path.analysis, trackID, splitID, analysisID, 'mat');

% PDF - Pattern EXPERIMENT.path.base/figure/<trackID>/<figureID>
EXPERIMENT.pattern.file.figure = @(trackID, figureID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.figure, trackID, figureID, 'pdf');

% TEX - Pattern EXPERIMENT.path.base/report/<trackID>/<reportID>
EXPERIMENT.pattern.file.report = @(trackID, reportID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.report, trackID, reportID, 'tex');


%% Patterns for identifiers

% Pattern <splitID>_s<smpl>
EXPERIMENT.pattern.identifier.split =  @(splitID, smpl) sprintf('%1$s_s%2$03d', splitID, smpl);

% Pattern <splitID>_shr<shr>_s<smpl>
EXPERIMENT.pattern.identifier.shard =  @(splitID, shr, smpl) sprintf('%1$s_shr%2$03d_s%3$03d', splitID, shr, smpl);

% Pattern pool_<corpusID>_<trackID>
% Pattern pool_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(partID, trackID) sprintf('pool_%1$s_%2$s', partID, trackID);

% Pattern run_<corpusID>_<trackID>
% Pattern run_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.run = @(partID, trackID) sprintf('run_%1$s_%2$s', partID, trackID);

% Pattern <mid>_<corpusID>_<trackID>
% Pattern <mid>_<shardID>_<trackID>
EXPERIMENT.pattern.identifier.measure =  @(mid, partID, trackID) sprintf('%1$s_%2$s_%3$s', mid, partID, trackID);

% Pattern <mdID>_<type>_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.general =  @(mdID, type, balanced, sstype, quartile, measureID, splitID, trackID) sprintf('%1$s_%2$s_%6$s_%3$s_sst%4$d_%5$s_%7$s_%8$s', mdID, type, balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_anova_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.analysis =  @(mdID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'anova', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_tbl_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.tbl =  @(mdID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'tbl', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_sts_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.sts =  @(mdID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'sts', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_blc_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.blc =  @(mdID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'blc', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.me =  @(mdID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'me', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_<measureID>_<factor>-me_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mePlot =  @(mdID, factor, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, [factor '-me'], balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_mc_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.mc =  @(mdID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'mc', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_soa_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.soa =  @(mdID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'soa', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern <mdID>_pwr_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.anova.pwr =  @(mdID, balanced, sstype, quartile, measureID, splitID, trackID) EXPERIMENT.pattern.identifier.anova.general(mdID, 'pwr', balanced, sstype, quartile, measureID, splitID, trackID);




% Pattern sigcnt_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.report.sigcounts = @(balanced, sstype, quartile, splitID, trackID) sprintf('sigcnt_%1$s_sst%2$d_%3$s_%4$s_%5$s', balanced, sstype, quartile, splitID, trackID);


% Pattern sigcnt_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.report.samples = @(balanced, sstype, quartile, splitID, trackID) sprintf('smpl_%1$s_sst%2$d_%3$s_%4$s_%5$s', balanced, sstype, quartile, splitID, trackID);


% Pattern me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.figure.me = @(balanced, sstype, quartile, measureID, splitID, trackID) sprintf('me_%4$s_%1$s_sst%2$d_%3$s_%5$s_%6$s', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.figure.me2 = @(balanced, sstype, quartile, measureID, splitID, trackID) sprintf('me2_%4$s_%1$s_sst%2$d_%3$s_%5$s_%6$s', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.figure.me3 = @(balanced, sstype, quartile, measureID, splitID, trackID) sprintf('me3_%4$s_%1$s_sst%2$d_%3$s_%5$s_%6$s', balanced, sstype, quartile, measureID, splitID, trackID);

% Pattern me_<measureID>_<balanced>_sst<sstype>_<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.figure.me4 = @(balanced, sstype, quartile, measureID, splitID, trackID) sprintf('me4_%4$s_%1$s_sst%2$d_%3$s_%5$s_%6$s', balanced, sstype, quartile, measureID, splitID, trackID);


% Pattern shard_stats_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.stats.shard =  @(splitID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', 'shard', 'stats', splitID, trackID);


% Pattern detailed_shard_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.shard =  @(splitID, trackID) sprintf('%1$s_%2$s_%3$s_%4$s', 'detailed', 'shard', splitID, trackID);

% Pattern <rqID>_detailed_anova_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.anova.detailed =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'detailed_anova', balanced, sstype, quartile, splitID, trackID);

% Pattern <rqID>_summary_anova_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.anova.summary =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'summary_anova', balanced, sstype, quartile, splitID, trackID);

% Pattern <rqID>_overall_anova_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.anova.overall =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'overall_anova', balanced, sstype, quartile, splitID, trackID);

% Pattern <rqID>_overall_anova_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.anova.counts =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'counts_anova', balanced, sstype, quartile, splitID, trackID);


% Pattern <rqID>_detailed_tukey_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.tukey.detailed =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'detailed_tukey', balanced, sstype, quartile, splitID, trackID);

% Pattern <rqID>_summary_tukey_<balanced>_sst<sstype>_q<quartile>_<splitID>_<trackID>
EXPERIMENT.pattern.identifier.rep.tukey.summary =  @(rqID, balanced, sstype, quartile, splitID, trackID) sprintf('%1$s_%2$s_%3$s_sst%4$d_q%5$d_%6$s_%7$s', rqID, 'summary_tukey', balanced, sstype, quartile, splitID, trackID);


%% Configuration for measures

% The list of measures under experimentation
EXPERIMENT.measure.list = {'ap', 'p10', 'rbp', 'ndcg'};
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';
EXPERIMENT.measure.ap.compute = @(pool, runSet, runLength) averagePrecision(pool, runSet);

% Configuration for P@10
EXPERIMENT.measure.p10.id = 'p10';
EXPERIMENT.measure.p10.acronym = 'P@10';
EXPERIMENT.measure.p10.name = 'Precision at 10 Retrieved Documents';
EXPERIMENT.measure.p10.cutoffs = 10;
EXPERIMENT.measure.p10.compute = @(pool, runSet, runLength) precision(pool, runSet, 'Cutoffs', EXPERIMENT.measure.p10.cutoffs);

% Configuration for RBP
EXPERIMENT.measure.rbp.id = 'rbp';
EXPERIMENT.measure.rbp.acronym = 'RBP';
EXPERIMENT.measure.rbp.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp.persistence = 0.8;
EXPERIMENT.measure.rbp.compute = @(pool, runSet, runLength) rankBiasedPrecision(pool, runSet, 'Persistence', EXPERIMENT.measure.rbp.persistence);

% Configuration for nDCG
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain at Last Retrieved Document';
EXPERIMENT.measure.ndcg.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.ndcg.logBase = 10;
EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg.compute = @(pool, runSet, runLength) discountedCumulatedGain(pool, runSet, 'CutOffs', EXPERIMENT.measure.ndcg.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', runLength, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 


%% Configuration for analyses

% The significance level for the analyses
EXPERIMENT.analysis.alpha.threshold = 0.05;
EXPERIMENT.analysis.alpha.color = 'lightgrey';

EXPERIMENT.analysis.smallEffect.threshold = 0.06;
EXPERIMENT.analysis.smallEffect.color = 'verylightblue';

EXPERIMENT.analysis.mediumEffect.threshold = 0.14;
EXPERIMENT.analysis.mediumEffect.color = 'lightblue';

EXPERIMENT.analysis.largeEffect.color = 'blue';

EXPERIMENT.analysis.label.subject = 'Topic';
EXPERIMENT.analysis.color.subject = rgb('FireBrick');

EXPERIMENT.analysis.label.factorA = 'System';
EXPERIMENT.analysis.color.factorA = rgb('RoyalBlue');

EXPERIMENT.analysis.label.factorB = 'Shard';
EXPERIMENT.analysis.color.factorB = rgb('ForestGreen');

% The list of the possible balancing types
% - if |unb| it will use an unbalanced design where missing values 
%   are denoted by |NaN|;
% - if |zero|, it will force a balanced design by substituting 
%   |NaN| values with zeros; 
% - if |one|, it will force a balanced design by substituting |NaN| values
%   with ones; 
% - if |med|, it will force a balanced design by substituting |NaN| values 
%   with the median value (ignoring |NaN|s) across all topics and systems; 
% - if |mean|, it will force a balanced design by substituting  |NaN| 
%   values with the mean value (ignoring |NaN|s) across all topics and 
%   systems; 
% - if |lq|, it will force a balanced design by substituting |NaN| values 
%   with the lower quartile value (ignoring |NaN|s) across all topics and 
%   systems;
% - if |uq|, it will force a balanced design by substituting |NaN| values 
%   with the upper quartile value (ignoring |NaN|s) across all topics and 
%   systems;
% - if |top|, keep only the topics with at least one relevant document for
% each shard;
% - if |b|, assume an already balanced design;
EXPERIMENT.analysis.balanced.list = {'unb', 'zero', 'one', 'mean', 'med', 'lq', 'uq', 'top', 'b'};

EXPERIMENT.analysis.balanced.unb.id = 'unb';
EXPERIMENT.analysis.balanced.unb.description  = 'Unbalanced design where missing values are denoted by NaN';
EXPERIMENT.analysis.balanced.unb.color = rgb('DarkSalmon');

EXPERIMENT.analysis.balanced.zero.id = 'zero';
EXPERIMENT.analysis.balanced.zero.description  = 'Balanced design where missing values are forced to zero';
EXPERIMENT.analysis.balanced.zero.color = rgb('FireBrick');

EXPERIMENT.analysis.balanced.one.id = 'one';
EXPERIMENT.analysis.balanced.one.description  = 'Balanced design where missing values are forced to one';
EXPERIMENT.analysis.balanced.one.color = rgb('Violet');

EXPERIMENT.analysis.balanced.mean.id = 'mean';
EXPERIMENT.analysis.balanced.mean.description  = 'Balanced design where missing values are forced to the mean value';
EXPERIMENT.analysis.balancedmeanone.color = rgb('Goldenrod');

EXPERIMENT.analysis.balanced.med.id = 'med';
EXPERIMENT.analysis.balanced.med.description  = 'Balanced design where missing values are forced to the median value';
EXPERIMENT.analysis.balanced.med.color = rgb('RoyalBlue');

EXPERIMENT.analysis.balanced.lq.id = 'lq';
EXPERIMENT.analysis.balanced.lq.description  = 'Balanced design where missing values are forced to the lower quartile value';
EXPERIMENT.analysis.balanced.lq.color = rgb('Orange');

EXPERIMENT.analysis.balanced.uq.id = 'uq';
EXPERIMENT.analysis.balanced.uq.description  = 'Balanced design where missing values are forced to the upper quartile value';
EXPERIMENT.analysis.balanced.uq.color = rgb('ForestGreen');

% The possible quartiles used in the experiments
EXPERIMENT.analysis.quartile.list = {'q1', 'q2', 'q3', 'q4'};
EXPERIMENT.analysis.quartile.q1.id = 'q1';
EXPERIMENT.analysis.quartile.q1.description  = 'Systems up to first quartile (top 25\%) of performance';

EXPERIMENT.analysis.quartile.q2.id = 'q2';
EXPERIMENT.analysis.quartile.q2.description  = 'Systems up to second quartile (top 50\%)/median of performance';

EXPERIMENT.analysis.quartile.q3.id = 'q3';
EXPERIMENT.analysis.quartile.q3.description  = 'Systems up to third quartile (top 75\%) of performance';

EXPERIMENT.analysis.quartile.q4.id = 'q4';
EXPERIMENT.analysis.quartile.q4.description  = 'All systems used';


% The list of the possible ANOVA analyses
EXPERIMENT.analysis.list = {'md1', 'md2', 'md3', 'md4', 'md5', 'md6'};

% Topic/System Effects on Whole Corpus
EXPERIMENT.analysis.md1.id = 'md1';
EXPERIMENT.analysis.md1.name = 'Topic/System Effects on Whole Corpus';
EXPERIMENT.analysis.md1.description = 'Crossed effects GLM on whole corpus: subjects are topics; effects are systems';
EXPERIMENT.analysis.md1.glmm = '$y_{ij} = \mu_{\cdot\cdot} + \tau_i + \alpha_j + \varepsilon_{ij}$';
% the model = Topic (subject) + System (factorA)
EXPERIMENT.analysis.md1.model = [1 0; ... 
                                 0 1];
EXPERIMENT.analysis.md1.compute = @(data, subject, factorA, sstype) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.md1.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    
EXPERIMENT.analysis.md1.color = rgb('Black');

    
% Topic/System Effects on Shards
EXPERIMENT.analysis.md2.id = 'md2';
EXPERIMENT.analysis.md2.name = 'Topic/System Effects on Shards';
EXPERIMENT.analysis.md2.description = 'Crossed effects GLM on shards: subjects are topics; effects are systems';
EXPERIMENT.analysis.md2.glmm = '$y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) 
EXPERIMENT.analysis.md2.model = EXPERIMENT.analysis.md1.model;
EXPERIMENT.analysis.md2.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.md2.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    
EXPERIMENT.analysis.md2.color = rgb('ForestGreen');
    
    

% Topic/System Effects and Topic*System Interaction on Shards 
EXPERIMENT.analysis.md3.id = 'md3';
EXPERIMENT.analysis.md3.name = 'Topic/System Effects and Topic*System Interaction on Shards';
EXPERIMENT.analysis.md3.description = 'Crossed effects GLM on shards: subjects are topics; effects are systems plus topic*system interaction';
EXPERIMENT.analysis.md3.glmm = '$y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + (\tau\alpha)_{ij} + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Topic*System
EXPERIMENT.analysis.md3.model = [EXPERIMENT.analysis.md2.model;
                                 1 1];
EXPERIMENT.analysis.md3.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA}, ...
        'Model', EXPERIMENT.analysis.md3.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    
EXPERIMENT.analysis.md3.color = rgb('Orange');
    
    
% Topic/System/Shard Effects and Topic*System Interaction on Shards
EXPERIMENT.analysis.md4.id = 'md4';
EXPERIMENT.analysis.md4.name = 'Topic/System/Shard Effects and Topic*System Interaction on Shards';
EXPERIMENT.analysis.md4.description = 'Crossed effects GLM on shards: subjects are topics; effects are systems and shards plus topic*system interaction';
EXPERIMENT.analysis.md4.glmm = '$y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k + (\tau\alpha)_{ij} + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Topic*System + 
%             Shards (factorB) 
EXPERIMENT.analysis.md4.model = [1 0 0; ... 
                                 0 1 0; ...
                                 1 1 0; ...
                                 0 0 1];
EXPERIMENT.analysis.md4.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.md4.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
EXPERIMENT.analysis.md4.color = rgb('RoyalBlue');                
    
    
% Topic/System/Shard Effects and Topic*System/System*Shard Interaction on Shards
EXPERIMENT.analysis.md5.id = 'md5';
EXPERIMENT.analysis.md5.name = 'Topic/System/Shard Effects and Topic*System/System*Shard Interaction on Shards';
EXPERIMENT.analysis.md5.description = 'Crossed effects GLM on shards: subjects are topics; effects are systems and shards plus topic*system and system*shard interactions';
EXPERIMENT.analysis.md5.glmm = '$t_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k + (\tau\alpha)_{ij} + (\alpha\beta)_{jk} + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Topic*System + 
%             Shards (factorB) + System*Shard
EXPERIMENT.analysis.md5.model = [EXPERIMENT.analysis.md4.model; ...                                
                                 0 1 1];
EXPERIMENT.analysis.md5.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.md5.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');
EXPERIMENT.analysis.md5.color = rgb('Goldenrod');  
    
    
% Topic/System/Shard Effects and Topic*System/System*Shard/Topic*Shard Interaction on Shards
EXPERIMENT.analysis.md6.id = 'md6';
EXPERIMENT.analysis.md6.name = 'Topic/System/Shard Effects and Topic*System/System*Shard/Topic*Shard Interactions on Shards';
EXPERIMENT.analysis.md6.description = 'Crossed effects GLM on shards: subjects are topics; effects are systems and shards plus topic*system, system*shard, and topic*shard interactions';
EXPERIMENT.analysis.md6.glmm = '$y_{ijk} = \mu_{\cdot\cdot\cdot} + \tau_i + \alpha_j + \beta_k + (\tau\alpha)_{ij} + (\alpha\beta)_{jk} + (\tau\beta)_{ik} + \varepsilon_{ijk}$';
% the model = Topic (subject) + System (factorA) + Topic*System + 
%             Shards (factorB) + System*Shard + Topic*Shard
EXPERIMENT.analysis.md6.model = [EXPERIMENT.analysis.md5.model; ...
                                 1 0 1];
EXPERIMENT.analysis.md6.compute = @(data, subject, factorA, factorB, sstype) anovan(data, {subject, factorA, factorB}, ...
        'Model', EXPERIMENT.analysis.md6.model, ...
        'VarNames', {EXPERIMENT.analysis.label.subject, EXPERIMENT.analysis.label.factorA, EXPERIMENT.analysis.label.factorB}, ...
        'sstype', sstype, 'alpha', EXPERIMENT.analysis.alpha.threshold, 'display', 'off');    
EXPERIMENT.analysis.md6.color = rgb('FireBrick');        
    
    
% Tukey HSD multiple comparison analysis for the system factor
EXPERIMENT.analysis.multcompare.system = @(sts) multcompare(sts, 'CType', 'hsd', 'Alpha', EXPERIMENT.analysis.alpha.threshold, 'dimension', [2], 'Display', 'off');

% Correlation among two rankings of systems
EXPERIMENT.analysis.corr = @(x, y) corr(x(:), y(:), 'type', 'Kendall');

