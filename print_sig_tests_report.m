%% print_sig_tests_report
% 
% Reports a summary about the ANOVA analyses and saves them to a |.tex|
% file.
%
%% Synopsis
%
%   [] = print_sig_tests_report(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|balanced|* - the type of design to enforce. if |unb| it
% will use an unbalanced design where missing values are denoted by |NaN|;
% if |zero|, it will force a balanced design by substituting |NaN| values 
% with zeros; if |med|, it will force a balanced design by substituting 
% |NaN| values with the median value (ignoring |NaN|s) across all topics
% and systems; if |mean|, it will force a balanced design by substituting 
% |NaN| values with the mean value (ignoring |NaN|s) across all topics
% and systems; if |lq|, it will force a balanced design by substituting 
% |NaN| values with the lower quartile value (ignoring |NaN|s) across all 
% topics and systems; if |uq|, it will force a balanced design by 
% substituting |NaN| values with the upper quartile value (ignoring |NaN|s)
% across all topics and systems; % if |one|, it will force a balanced 
% design by substituting |NaN| values with zeros; 
% * *|sstype|* - the type of sum of squares in ANOVA. Use 3 for a typical
% balanced design.
% * *|quartile|* - the quartile of systems to be analysed. Use q1 for top
% quartile; q2 for median; q3 for up to third quartile; q4 for all systems.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing
%

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017a or higher
% * *Copyright:* (C) 2018-2019 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>


%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

function [] = print_sig_tests_report(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)

    persistent MD1 MD2  MD3 MD4 MD5 MD6;

    if isempty(MD1)
        MD1 = 'md1';
        MD2 = 'md2';
        MD3 = 'md3';
        MD4 = 'md4';
        MD5 = 'md5';
        MD6 = 'md6';
    end

    % check the number of input parameters
    narginchk(5, 7);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');

    % check that splitID is a non-empty cell array
    validateattributes(splitID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');

    if iscell(splitID)
        % check that splitID is a cell array of strings with one element
        assert(iscellstr(splitID) && numel(splitID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    splitID = char(strtrim(splitID));
    splitID = splitID(:).';

    % check that splitID assumes a valid value
    validatestring(splitID, ...
        EXPERIMENT.split.list, '', 'splitID');

    % check that the track and the split rely on the same corpus
    assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(splitID).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, splitID);

    % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');

    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';

    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');

    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
        {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');

    % check that quartile is a non-empty cell array
    validateattributes(quartile, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'quartile');

    if iscell(quartile)
        % check that quartile is a cell array of strings with one element
        assert(iscellstr(quartile) && numel(quartile) == 1, ...
            'MATTERS:IllegalArgument', 'Expected quartile to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    quartile = char(strtrim(quartile));
    quartile = quartile(:).';

    % check that quartile assumes a valid value
    validatestring(quartile, ...
        EXPERIMENT.analysis.quartile.list, '', 'quartile');


    if nargin == 7
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');

        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;


    fprintf('\n\n######## Reporting summary ANOVA analyses on track %s (%s) ########\n\n', ...
        trackID, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %s - %s \n', quartile, EXPERIMENT.analysis.quartile.(quartile).description);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', splitID);
    fprintf('    * shard(s): %d\n', EXPERIMENT.split.(splitID).shard);
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));            

        
    fprintf('+ Printing the report\n');
    fprintf('  ');
    
    % the file where the report has to be written
    reportID = EXPERIMENT.pattern.identifier.report.sigcounts(balanced, sstype, quartile, splitID, trackID);
    fid = fopen(EXPERIMENT.pattern.file.report(trackID, reportID), 'w');


    fprintf(fid, '\\documentclass[11pt]{article} \n\n');

    fprintf(fid, '\\usepackage{amsmath}\n');
    fprintf(fid, '\\usepackage{multirow}\n');
    fprintf(fid, '\\usepackage{longtable}\n');
    fprintf(fid, '\\usepackage{colortbl}\n');
    fprintf(fid, '\\usepackage{lscape}\n');
    fprintf(fid, '\\usepackage{pdflscape}\n');
    fprintf(fid, '\\usepackage{rotating}\n');
    fprintf(fid, '\\usepackage[a3paper]{geometry}\n\n');
    
    fprintf(fid, '\\usepackage{xcolor}\n');
    fprintf(fid, '\\definecolor{lightgrey}{RGB}{219, 219, 219}\n');
    fprintf(fid, '\\definecolor{verylightblue}{RGB}{204, 229, 255}\n');
    fprintf(fid, '\\definecolor{lightblue}{RGB}{124, 216, 255}\n');
    fprintf(fid, '\\definecolor{blue}{RGB}{32, 187, 253}\n');
    
    fprintf(fid, '\\begin{document}\n\n');
    
    
    fprintf(fid, '\\title{Report on ANOVA Analyses \\\\ on %s - %s }\n\n', ...
        strrep(trackID, '_', '\_'), strrep(splitID, '_', '\_'));
    
    fprintf(fid, '\\author{Nicola Ferro}\n\n');
    
    fprintf(fid, '\\maketitle\n\n');
    
    
    fprintf(fid, 'Settings:\n');
    fprintf(fid, '\\begin{itemize}\n');
    
    fprintf(fid, '\\item track: %s -- %s \n', strrep(trackID, '_', '\_'), EXPERIMENT.track.(trackID).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item topics: %d \n', EXPERIMENT.track.(trackID).topics);
    fprintf(fid, '\\item runs: %d \n', EXPERIMENT.track.(trackID).runs);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item split: %s \n',strrep(splitID, '_', '\_'));    
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item corpus: %s \n', strrep(EXPERIMENT.split.(splitID).corpus, '_', '\_'));
    fprintf(fid, '\\item description: %s \n', EXPERIMENT.split.(splitID).name);
    fprintf(fid, '\\item shards: %d \n', EXPERIMENT.split.(splitID).shard);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item analysis:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    fprintf(fid, '\\item quartile: %s -- %s \n', quartile, EXPERIMENT.analysis.quartile.(quartile).description);
    fprintf(fid, '\\item ANOVA balancing type: \\texttt{%s} -- %s\n', balanced, EXPERIMENT.analysis.balanced.(balanced).description);    
    fprintf(fid, '\\item ANOVA sum of squares type: %d\n', sstype);
    fprintf(fid, '\\item Tukey HSD multiple comparison test\n');
    fprintf(fid, '\\item significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item ANOVA models:\n');
    fprintf(fid, '\\begin{itemize}\n');
    
    fprintf(fid, '\\item %s: %s \n', MD1, EXPERIMENT.analysis.(MD1).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item model: %s\n', EXPERIMENT.analysis.(MD1).glmm);
    fprintf(fid, '\\item description: %s\n', EXPERIMENT.analysis.(MD1).description);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item %s: %s \n', MD2, EXPERIMENT.analysis.(MD2).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item model: %s\n', EXPERIMENT.analysis.(MD2).glmm);
    fprintf(fid, '\\item description: %s\n', EXPERIMENT.analysis.(MD2).description);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item %s: %s \n', MD3, EXPERIMENT.analysis.(MD3).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item model: %s\n', EXPERIMENT.analysis.(MD3).glmm);
    fprintf(fid, '\\item description: %s\n', EXPERIMENT.analysis.(MD3).description);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item %s: %s \n', MD4, EXPERIMENT.analysis.(MD4).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item model: %s\n', EXPERIMENT.analysis.(MD4).glmm);
    fprintf(fid, '\\item description: %s\n', EXPERIMENT.analysis.(MD4).description);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item %s: %s \n', MD5, EXPERIMENT.analysis.(MD5).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item model: %s\n', EXPERIMENT.analysis.(MD5).glmm);
    fprintf(fid, '\\item description: %s\n', EXPERIMENT.analysis.(MD5).description);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\item %s: %s \n', MD6, EXPERIMENT.analysis.(MD6).name);
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item model: %s\n', EXPERIMENT.analysis.(MD6).glmm);
    fprintf(fid, '\\item description: %s\n', EXPERIMENT.analysis.(MD6).description);
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\end{itemize}\n');
           
    fprintf(fid, '\\item analysed measures:\n');
    fprintf(fid, '\\begin{itemize}\n');
    for m = startMeasure:endMeasure        
        fprintf(fid, '\\item %s: %s\n', ...
            strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), EXPERIMENT.measure.getName(m));        
    end
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\end{itemize}\n\n');
                     
    
    fprintf(fid, 'For each measure, the tables report:\n');
    fprintf(fid, '\\begin{itemize}\n');
    fprintf(fid, '\\item \\textbf{$\\hat{\\omega}_{\\langle fact\\rangle}^2$}: the effect size of the factor;\n');
    fprintf(fid, '\\item \\textbf{Pwr}: the power for the statistical significance testing of the factor;\n');
    fprintf(fid, '\\item \\textbf{Sig}: the total number of significantly different systems;\n');
    fprintf(fid, '\\item \\textbf{NotSig}: the total number of not significantly different systems;\n');
    fprintf(fid, '\\item \\textbf{TopG}: the total number of systems in the top group, i.e. the group of the systems not significantly different from the top performing one;\n');
    fprintf(fid, '\\item \\textbf{Sig2NotSig}: number of pairs changed from significantly to not significantly different when passing from one ANOVA model to another;\n');
    fprintf(fid, '\\item \\textbf{NotSig2Sig}: number of pairs changed from not significantly to significantly different when passing from one ANOVA model to another;\n');
	fprintf(fid, '\\item \\textbf{Sig2Sig}: number of pairs which are significantly different for both ANOVA models;\n');
    fprintf(fid, '\\item \\textbf{NotSig2NotSig}: number of pairs which are not significantly different for both ANOVA models;\n');
    fprintf(fid, '\\item \\textbf{pInc}: number of pairs for which the p-value has increased when passing from from one ANOVA model to another; this denotes a tendency to move towards not significantly different;\n');
    fprintf(fid, '\\item \\textbf{pDec}: number of pairs for which the p-value has decreased when passing from from one ANOVA model to another; this denotes a tendency to move towards significantly different;\n');
	fprintf(fid, '\\item \\textbf{pEq}: number of pairs for which the p-value stayed equal for both ANOVA models;\n');
    fprintf(fid, '\\item \\textbf{tgIn}: number of system which entered the top group when passing from from one ANOVA model to another;\n');
	fprintf(fid, '\\item \\textbf{tgOut}: number of system which exited the top group when passing from from one ANOVA model to another;\n');
    fprintf(fid, '\\item \\textbf{tgEq}: number of system which stayed in the top group for both ANOVA models;\n');
	fprintf(fid, '\\end{itemize}\n\n');


    fprintf(fid, 'Note that the comparisons have to be read from right to left (sorry for this). The table header reports Model, e.g. md2, vs Model, eg. md1. In this case, for example, \\textbf{Sig2NotSig} means how many pairs where significantly different in md1 and they are not in md2.\n\n');
    
    fprintf(fid, '\\vspace*{1em}Rule of thumb for effect size $\\hat{\\omega}_{\\langle fact\\rangle}^2$:\n');
    fprintf(fid, '\\begin{itemize}\n');    
    fprintf(fid, '\\item large effect: $\\hat{\\omega}_{\\langle fact\\rangle}^2 \\geq 0.14$\n');
    fprintf(fid, '\\item medium effect: $0.06 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.14$\n');
    fprintf(fid, '\\item small effect: $0.01 \\leq \\hat{\\omega}_{\\langle fact\\rangle}^2 < 0.06$\n');
    fprintf(fid, '\\item negative values have to be considered as $0$\n');
    fprintf(fid, '\\end{itemize}\n');
    
    fprintf(fid, '\\newpage\n');
    
  
    for m = startMeasure:endMeasure
        
        mid = EXPERIMENT.measure.list{m};
        
        % load MD1 data        
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MD1, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID).corpus, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(MD1, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID).corpus, trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(MD1, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID).corpus, trackID);
        anovaPwrID = EXPERIMENT.pattern.identifier.anova.pwr(MD1, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID).corpus, trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(MD1, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID).corpus, trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(MD1, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID).corpus, trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis.corpus(trackID, anovaID), ...
                'WorkspaceVarNames', {'md1_me', 'md1_mc', 'md1_pwr', 'md1_soa', 'md1_blc'}, ...
                'FileVarNames', {anovaMeID, anovaMcID, anovaPwrID, anovaSoAID, anovaBlcID});
        
        % load MD2 data
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MD2, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(MD2, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(MD2, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaPwrID = EXPERIMENT.pattern.identifier.anova.pwr(MD2, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(MD2, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(MD2, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, splitID, anovaID), ...
            'WorkspaceVarNames', {'md2_me', 'md2_mc', 'md2_pwr', 'md2_soa', 'md2_blc'}, ...
            'FileVarNames', {anovaMeID, anovaMcID, anovaPwrID, anovaSoAID, anovaBlcID});        
        
        % load MD3 data
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MD3, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(MD3, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(MD3, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaPwrID = EXPERIMENT.pattern.identifier.anova.pwr(MD3, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(MD3, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(MD3, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, splitID, anovaID), ...
            'WorkspaceVarNames', {'md3_me', 'md3_mc', 'md3_pwr', 'md3_soa', 'md3_blc'}, ...
            'FileVarNames', {anovaMeID, anovaMcID, anovaPwrID, anovaSoAID, anovaBlcID});        
            

        % load MD4 data
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MD4, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(MD4, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(MD4, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaPwrID = EXPERIMENT.pattern.identifier.anova.pwr(MD4, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(MD4, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(MD4, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, splitID, anovaID), ...
            'WorkspaceVarNames', {'md4_me', 'md4_mc', 'md4_pwr', 'md4_soa', 'md4_blc'}, ...
            'FileVarNames', {anovaMeID, anovaMcID, anovaPwrID, anovaSoAID, anovaBlcID});        
        

        % load MD5 data
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MD5, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(MD5, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(MD5, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaPwrID = EXPERIMENT.pattern.identifier.anova.pwr(MD5, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(MD5, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(MD5, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, splitID, anovaID), ...
            'WorkspaceVarNames', {'md5_me', 'md5_mc', 'md5_pwr', 'md5_soa', 'md5_blc'}, ...
            'FileVarNames', {anovaMeID, anovaMcID, anovaPwrID, anovaSoAID, anovaBlcID});        
        

        % load MD6 data
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(MD6, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(MD6, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaMcID = EXPERIMENT.pattern.identifier.anova.mc(MD6, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaPwrID = EXPERIMENT.pattern.identifier.anova.pwr(MD6, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaSoAID = EXPERIMENT.pattern.identifier.anova.soa(MD6, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        anovaBlcID = EXPERIMENT.pattern.identifier.anova.blc(MD6, balanced, sstype, quartile, mid, EXPERIMENT.pattern.identifier.split(splitID, 1), trackID);
        
        serload2(EXPERIMENT.pattern.file.analysis.shard(trackID, splitID, anovaID), ...
            'WorkspaceVarNames', {'md6_me', 'md6_mc', 'md6_pwr', 'md6_soa', 'md6_blc'}, ...
            'FileVarNames', {anovaMeID, anovaMcID, anovaPwrID, anovaSoAID, anovaBlcID});        
        
        
        assert( (size(md1_mc, 1) == size(md2_mc, 1)) & ...
                (size(md2_mc, 1) == size(md3_mc, 1)) & ...
                (size(md3_mc, 1) == size(md4_mc, 1)) & ...
                (size(md4_mc, 1) == size(md5_mc, 1)) & ...
                (size(md5_mc, 1) == size(md6_mc, 1)), ...
            'MATTERS:IllegalState', 'System pairs expected to be equal.');
        
        assert( (md2_me.factorA.tau == md3_me.factorA.tau) & ...
            (md3_me.factorA.tau == md4_me.factorA.tau) & ...
            (md4_me.factorA.tau == md5_me.factorA.tau) & ...
            (md5_me.factorA.tau == md6_me.factorA.tau), ...
            'MATTERS:IllegalState', 'Correlations expected to be equal.');
        
        
        assert( (md2_blc.value == md3_blc.value) & ...
            (md3_blc.value == md4_blc.value) & ...
            (md4_blc.value == md5_blc.value) & ...
            (md5_blc.value == md6_blc.value), ...
            'MATTERS:IllegalState', 'Balancing values expected to be equal.');
        
        
        fprintf(fid, '\\begin{table*}[tbp] \n');
        
        fprintf(fid, '\\centering \n');
                
        fprintf(fid, '\\caption{Summary of ANOVA analyses for %s on track \\texttt{%s} with split \\texttt{%s}. Kendall''s $\\tau$ correlation %5.4f among ranking of systems on the whole collection (%s) and on shards (%s, %s, %s, %s, and %s). %d system pairs analysed. Balancing type \\texttt{%s} with value %5.4f.}\n', ...
            strrep(EXPERIMENT.measure.getAcronym(m), '_', '\_'), ...
            strrep(trackID, '_', '\_'), strrep(splitID, '_', '\_'), ...
            md2_me.factorA.tau, upper(MD1), upper(MD2), upper(MD3), upper(MD4), upper(MD5), upper(MD6), ...
            size(md1_mc, 1), md2_blc.type, md2_blc.value);
        
        fprintf(fid, '\\label{tab:smrysigcnts-%s-%s-%s}\n', mid, trackID, splitID);
        fprintf(fid, '\\small \n');
        fprintf(fid, '\\hspace*{-5em} \n');
        fprintf(fid, '\\begin{tabular}{|l|l||r|r|r|r|r||r|r|r|r||r|r|r||r|r|r|} \n');
        
        fprintf(fid, '\\hline\\hline \n');
        
        fprintf(fid, '\\multicolumn{1}{|c|}{\\textbf{Model}} & \\multicolumn{1}{c||}{\\textbf{vs Model}} & \\multicolumn{1}{c|}{\\textbf{$\\hat{\\omega}_{\\langle fact\\rangle}^2$}} & \\multicolumn{1}{c|}{\\textbf{Pwr}} & \\multicolumn{1}{c|}{\\textbf{Sig}} & \\multicolumn{1}{c|}{\\textbf{NotSig}} & \\multicolumn{1}{c||}{\\textbf{TopG}} & \\multicolumn{1}{c|}{\\textbf{Sig2NotSig}} & \\multicolumn{1}{c|}{\\textbf{NotSig2Sig}} & \\multicolumn{1}{c|}{\\textbf{Sig2Sig}} & \\multicolumn{1}{c||}{\\textbf{NotSig2NotSig}} & \\multicolumn{1}{c|}{\\textbf{pInc}} & \\multicolumn{1}{c|}{\\textbf{pDec}} & \\multicolumn{1}{c||}{\\textbf{pEq}} & \\multicolumn{1}{c|}{\\textbf{tgIn}} & \\multicolumn{1}{c|}{\\textbf{tgOut}} & \\multicolumn{1}{c|}{\\textbf{tgEq}} \\\\ \n');
        
        fprintf(fid, '\\hline\\hline \n');
        
        p_md1 = md1_mc(:, 6);
        md1_sig = p_md1 <= EXPERIMENT.analysis.alpha.threshold;
        eps_p_md1 = eps(p_md1);
        
        fprintf(fid, '%s & -- & %5.4f & %5.4f & %d & %d & %d & -- & -- & -- & -- & -- & -- & -- & -- & -- & --  \\\\ \n', ...
            upper(MD1), md1_soa.omega2p.factorA, md1_pwr.factorA.power, sum(md1_sig), sum(~md1_sig), length(md1_me.factorA.tg));
        
        
        assert(size(md1_mc, 1) ==  sum(md1_sig) + sum(~md1_sig), '%s sig and notsig counts are not equal: %d', MD1, size(md1_mc, 1), sum(md1_sig) + sum(~md1_sig));
        
        fprintf(fid, '\\hline\\hline \n');

        
        p_md2 = md2_mc(:, 6);
        md2_sig = p_md2 <= EXPERIMENT.analysis.alpha.threshold;
        eps_p_md2 = eps(p_md2);
        
        fprintf(fid, '%s & -- & %5.4f & %5.4f & %d & %d & %d & -- & -- & -- & -- & -- & -- & -- & -- & -- & --  \\\\ \n', ...
            upper(MD2), md2_soa.omega2p.factorA, md2_pwr.factorA.power, sum(md2_sig), sum(~md2_sig), length(md2_me.factorA.tg));
        
        assert(size(md1_mc, 1) ==  sum(md2_sig) + sum(~md2_sig), '%s sig and notsig counts are not equal: %d', MD2, size(md1_mc, 1), sum(md2_sig) + sum(~md2_sig));
        
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md1) + eps(eps_p_md2);        
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD1), sum(md1_sig & ~md2_sig), sum(~md1_sig & md2_sig), sum(md1_sig & md2_sig), sum(~md1_sig & ~md2_sig), ...
            sum(p_md2 - p_md1 > diffeps), sum(p_md1 - p_md2 > diffeps), sum(abs(p_md2 - p_md1) <= diffeps), ...
            length(setdiff(md2_me.factorA.tg, md1_me.factorA.tg)), length(setdiff(md1_me.factorA.tg, md2_me.factorA.tg)), length(intersect(md1_me.factorA.tg, md2_me.factorA.tg)));
        
        assert(size(md1_mc, 1) == sum(md1_sig & ~md2_sig) + sum(~md1_sig & md2_sig) + sum(md1_sig & md2_sig) + sum(~md1_sig & ~md2_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD2, MD1, size(md1_mc, 1), sum(md1_sig & ~md2_sig) + sum(~md1_sig & md2_sig) + sum(md1_sig & md2_sig) + sum(~md1_sig & ~md2_sig));
        assert(size(md1_mc, 1) ==  sum(p_md2 - p_md1 > diffeps) + sum(p_md1 - p_md2 > diffeps) + sum(abs(p_md2 - p_md1) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD2, MD1, size(md1_mc, 1), sum(p_md2 - p_md1 > diffeps) + sum(p_md1 - p_md2 > diffeps) + sum(abs(p_md2 - p_md1) <= diffeps));
        
        fprintf(fid, '\\hline\\hline \n');

        
        p_md3 = md3_mc(:, 6);
        md3_sig = p_md3 <= EXPERIMENT.analysis.alpha.threshold;
        eps_p_md3 = eps(p_md3);
        
        fprintf(fid, '%s & -- & %5.4f & %5.4f & %d & %d & %d & -- & -- & -- & -- & -- & -- & -- & -- & -- & --  \\\\ \n', ...
            upper(MD3), md3_soa.omega2p.factorA, md3_pwr.factorA.power, sum(md3_sig), sum(~md3_sig), length(md3_me.factorA.tg));
        
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md1) + eps(eps_p_md3);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD1), sum(md1_sig & ~md3_sig), sum(~md1_sig & md3_sig), sum(md1_sig & md3_sig), sum(~md1_sig & ~md3_sig), ...
            sum(p_md3 - p_md1 > diffeps), sum(p_md1 - p_md3 > diffeps), sum(abs(p_md3 - p_md1) <= diffeps), ...
            length(setdiff(md3_me.factorA.tg, md1_me.factorA.tg)), length(setdiff(md1_me.factorA.tg, md3_me.factorA.tg)), length(intersect(md1_me.factorA.tg, md3_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md1_sig & ~md3_sig) + sum(~md1_sig & md3_sig) + sum(md1_sig & md3_sig) + sum(~md1_sig & ~md3_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD3, MD1, size(md1_mc, 1), sum(md1_sig & ~md3_sig) + sum(~md1_sig & md3_sig) + sum(md1_sig & md3_sig) + sum(~md1_sig & ~md3_sig));
        assert(size(md1_mc, 1) ==  sum(p_md3 - p_md1 > diffeps) + sum(p_md1 - p_md3 > diffeps) + sum(abs(p_md3 - p_md1) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD3, MD1, size(md1_mc, 1), sum(p_md3 - p_md1 > diffeps) + sum(p_md1 - p_md3 > diffeps) + sum(abs(p_md3 - p_md1) <= diffeps));
            
        fprintf(fid, '\\cline{2-17} \n');
        
        diffeps = eps(eps_p_md2) + eps(eps_p_md3);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD2), sum(md2_sig & ~md3_sig), sum(~md2_sig & md3_sig), sum(md2_sig & md3_sig), sum(~md2_sig & ~md3_sig), ...
            sum(p_md3 - p_md2 > diffeps), sum(p_md2 - p_md3 > diffeps), sum(abs(p_md3 - p_md2) <= diffeps), ...
            length(setdiff(md3_me.factorA.tg, md2_me.factorA.tg)), length(setdiff(md2_me.factorA.tg, md3_me.factorA.tg)), length(intersect(md2_me.factorA.tg, md3_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md2_sig & ~md3_sig) + sum(~md2_sig & md3_sig) + sum(md2_sig & md3_sig) + sum(~md2_sig & ~md3_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD3, MD2, size(md1_mc, 1), sum(md2_sig & ~md3_sig) + sum(~md2_sig & md3_sig) + sum(md2_sig & md3_sig) + sum(~md2_sig & ~md3_sig));
        assert(size(md1_mc, 1) ==  sum(p_md3 - p_md2 > diffeps) + sum(p_md2 - p_md3 > diffeps) + sum(abs(p_md3 - p_md2) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD3, MD2, size(md1_mc, 1), sum(p_md3 - p_md2 > diffeps) + sum(p_md2 - p_md3 > diffeps) + sum(abs(p_md3 - p_md2) <= diffeps));
        
        fprintf(fid, '\\hline\\hline \n');

        
        p_md4 = md4_mc(:, 6);
        md4_sig = p_md4 <= EXPERIMENT.analysis.alpha.threshold;
        eps_p_md4 = eps(p_md4);
        
        fprintf(fid, '%s & -- & %5.4f & %5.4f & %d & %d & %d & -- & -- & -- & -- & -- & -- & -- & -- & -- & --  \\\\ \n', ...
            upper(MD4), md4_soa.omega2p.factorA, md4_pwr.factorA.power, sum(md4_sig), sum(~md4_sig), length(md4_me.factorA.tg));
        
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md1) + eps(eps_p_md4);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD1), sum(md1_sig & ~md4_sig), sum(~md1_sig & md4_sig), sum(md1_sig & md4_sig), sum(~md1_sig & ~md4_sig), ...
            sum(p_md4 - p_md1 > diffeps), sum(p_md1 - p_md4 > diffeps), sum(abs(p_md4 - p_md1) <= diffeps), ...
            length(setdiff(md4_me.factorA.tg, md1_me.factorA.tg)), length(setdiff(md1_me.factorA.tg, md4_me.factorA.tg)), length(intersect(md1_me.factorA.tg, md4_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md1_sig & ~md4_sig) + sum(~md1_sig & md4_sig) + sum(md1_sig & md4_sig) + sum(~md1_sig & ~md4_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD4, MD1, size(md1_mc, 1), sum(md1_sig & ~md4_sig) + sum(~md1_sig & md4_sig) + sum(md1_sig & md4_sig) + sum(~md1_sig & ~md4_sig));
        assert(size(md1_mc, 1) ==  sum(p_md4 - p_md1 > diffeps) + sum(p_md1 - p_md4 > diffeps) + sum(abs(p_md4 - p_md1) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD4, MD1, size(md1_mc, 1), sum(p_md4 - p_md1 > diffeps) + sum(p_md1 - p_md4 > diffeps) + sum(abs(p_md4 - p_md1) <= diffeps));
                                       
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md2) + eps(eps_p_md4);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD2), sum(md2_sig & ~md4_sig), sum(~md2_sig & md4_sig), sum(md2_sig & md4_sig), sum(~md2_sig & ~md4_sig), ...
            sum(p_md4 - p_md2 > diffeps), sum(p_md2 - p_md4 > diffeps), sum(abs(p_md4 - p_md2) <= diffeps), ...
            length(setdiff(md4_me.factorA.tg, md2_me.factorA.tg)), length(setdiff(md2_me.factorA.tg, md4_me.factorA.tg)), length(intersect(md2_me.factorA.tg, md4_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md2_sig & ~md4_sig) + sum(~md2_sig & md4_sig) + sum(md2_sig & md4_sig) + sum(~md2_sig & ~md4_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD4, MD2, size(md1_mc, 1), sum(md2_sig & ~md4_sig) + sum(~md2_sig & md4_sig) + sum(md2_sig & md4_sig) + sum(~md2_sig & ~md4_sig));
        assert(size(md1_mc, 1) ==  sum(p_md4 - p_md2 > diffeps) + sum(p_md2 - p_md4 > diffeps) + sum(abs(p_md4 - p_md2) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD4, MD2, size(md1_mc, 1), sum(p_md4 - p_md2 > diffeps) + sum(p_md2 - p_md4 > diffeps) + sum(abs(p_md4 - p_md2) <= diffeps));
        
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md3) + eps(eps_p_md4);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD3), sum(md3_sig & ~md4_sig), sum(~md3_sig & md4_sig), sum(md3_sig & md4_sig), sum(~md3_sig & ~md4_sig), ...
            sum(p_md4 - p_md3 > diffeps), sum(p_md3 - p_md4 > diffeps), sum(abs(p_md4 - p_md3) <= diffeps), ...
            length(setdiff(md4_me.factorA.tg, md3_me.factorA.tg)), length(setdiff(md3_me.factorA.tg, md4_me.factorA.tg)), length(intersect(md3_me.factorA.tg, md4_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md3_sig & ~md4_sig) + sum(~md3_sig & md4_sig) + sum(md3_sig & md4_sig) + sum(~md3_sig & ~md4_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD4, MD3, size(md1_mc, 1), sum(md3_sig & ~md4_sig) + sum(~md3_sig & md4_sig) + sum(md3_sig & md4_sig) + sum(~md3_sig & ~md4_sig));
        assert(size(md1_mc, 1) ==  sum(p_md4 - p_md3 > diffeps) + sum(p_md3 - p_md4 > diffeps) + sum(abs(p_md4 - p_md3) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD4, MD3, size(md1_mc, 1), sum(p_md4 - p_md3 > diffeps) + sum(p_md3 - p_md4 > diffeps) + sum(abs(p_md4 - p_md3) <= diffeps));

        fprintf(fid, '\\hline\\hline \n');

        
        p_md5 = md5_mc(:, 6);
        md5_sig = p_md5 <= EXPERIMENT.analysis.alpha.threshold;
        eps_p_md5 = eps(p_md5);
        
        fprintf(fid, '%s & -- & %5.4f & %5.4f & %d & %d & %d & -- & -- & -- & -- & -- & -- & -- & -- & -- & --  \\\\ \n', ...
            upper(MD5), md5_soa.omega2p.factorA, md5_pwr.factorA.power, sum(md5_sig), sum(~md5_sig), length(md5_me.factorA.tg));
        
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md1) + eps(eps_p_md5);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD1), sum(md1_sig & ~md5_sig), sum(~md1_sig & md5_sig), sum(md1_sig & md5_sig), sum(~md1_sig & ~md5_sig), ...
            sum(p_md5 - p_md1 > diffeps), sum(p_md1 - p_md5 > diffeps), sum(abs(p_md5 - p_md1) <= diffeps), ...
            length(setdiff(md5_me.factorA.tg, md1_me.factorA.tg)), length(setdiff(md1_me.factorA.tg, md5_me.factorA.tg)), length(intersect(md1_me.factorA.tg, md5_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md1_sig & ~md5_sig) + sum(~md1_sig & md5_sig) + sum(md1_sig & md5_sig) + sum(~md1_sig & ~md5_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD5, MD1, size(md1_mc, 1), sum(md1_sig & ~md5_sig) + sum(~md1_sig & md5_sig) + sum(md1_sig & md5_sig) + sum(~md1_sig & ~md5_sig));
        assert(size(md1_mc, 1) ==  sum(p_md5 - p_md1 > diffeps) + sum(p_md1 - p_md5 > diffeps) + sum(abs(p_md5 - p_md1) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD5, MD1, size(md1_mc, 1), sum(p_md5 - p_md1 > diffeps) + sum(p_md1 - p_md5 > diffeps) + sum(abs(p_md5 - p_md1) <= diffeps));

        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md2) + eps(eps_p_md5);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD2), sum(md2_sig & ~md5_sig), sum(~md2_sig & md5_sig), sum(md2_sig & md5_sig), sum(~md2_sig & ~md5_sig), ...
            sum(p_md5 - p_md2 > diffeps), sum(p_md2 - p_md5 > diffeps), sum(abs(p_md5 - p_md2) <= diffeps), ...
            length(setdiff(md5_me.factorA.tg, md2_me.factorA.tg)), length(setdiff(md2_me.factorA.tg, md5_me.factorA.tg)), length(intersect(md2_me.factorA.tg, md5_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md2_sig & ~md5_sig) + sum(~md2_sig & md5_sig) + sum(md2_sig & md5_sig) + sum(~md2_sig & ~md5_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD5, MD2, size(md1_mc, 1), sum(md2_sig & ~md5_sig) + sum(~md2_sig & md5_sig) + sum(md2_sig & md5_sig) + sum(~md2_sig & ~md5_sig));
        assert(size(md1_mc, 1) ==  sum(p_md5 - p_md2 > diffeps) + sum(p_md2 - p_md5 > diffeps) + sum(abs(p_md5 - p_md2) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD5, MD2, size(md1_mc, 1), sum(p_md5 - p_md2 > diffeps) + sum(p_md2 - p_md5 > diffeps) + sum(abs(p_md5 - p_md2) <= diffeps));

        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md3) + eps(eps_p_md5);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD3), sum(md3_sig & ~md5_sig), sum(~md3_sig & md5_sig), sum(md3_sig & md5_sig), sum(~md3_sig & ~md5_sig), ...
            sum(p_md5 - p_md3 > diffeps), sum(p_md3 - p_md5 > diffeps), sum(abs(p_md5 - p_md3) <= diffeps), ...
            length(setdiff(md5_me.factorA.tg, md3_me.factorA.tg)), length(setdiff(md3_me.factorA.tg, md5_me.factorA.tg)), length(intersect(md3_me.factorA.tg, md5_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md3_sig & ~md5_sig) + sum(~md3_sig & md5_sig) + sum(md3_sig & md5_sig) + sum(~md3_sig & ~md5_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD5, MD3, size(md1_mc, 1), sum(md3_sig & ~md5_sig) + sum(~md3_sig & md5_sig) + sum(md3_sig & md5_sig) + sum(~md3_sig & ~md5_sig));
        assert(size(md1_mc, 1) ==  sum(p_md5 - p_md3 > diffeps) + sum(p_md3 - p_md5 > diffeps) + sum(abs(p_md5 - p_md3) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD5, MD3, size(md1_mc, 1), sum(p_md5 - p_md3 > diffeps) + sum(p_md3 - p_md5 > diffeps) + sum(abs(p_md5 - p_md3) <= diffeps));

        fprintf(fid, '\\cline{2-17} \n');
        
        diffeps = eps(eps_p_md4) + eps(eps_p_md5);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD4), sum(md4_sig & ~md5_sig), sum(~md4_sig & md5_sig), sum(md4_sig & md5_sig), sum(~md4_sig & ~md5_sig), ...
            sum(p_md5 - p_md4 > diffeps), sum(p_md4 - p_md5 > diffeps), sum(abs(p_md5 - p_md4) <= diffeps), ...
            length(setdiff(md5_me.factorA.tg, md4_me.factorA.tg)), length(setdiff(md4_me.factorA.tg, md5_me.factorA.tg)), length(intersect(md4_me.factorA.tg, md5_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md4_sig & ~md5_sig) + sum(~md4_sig & md5_sig) + sum(md4_sig & md5_sig) + sum(~md4_sig & ~md5_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD5, MD4, size(md1_mc, 1), sum(md4_sig & ~md5_sig) + sum(~md4_sig & md5_sig) + sum(md4_sig & md5_sig) + sum(~md4_sig & ~md5_sig));
        assert(size(md1_mc, 1) ==  sum(p_md5 - p_md4 > diffeps) + sum(p_md4 - p_md5 > diffeps) + sum(abs(p_md5 - p_md4) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD5, MD4, size(md1_mc, 1), sum(p_md5 - p_md4 > diffeps) + sum(p_md4 - p_md5 > diffeps) + sum(abs(p_md5 - p_md4) <= diffeps));
                            
        fprintf(fid, '\\hline\\hline \n');

        
        p_md6 = md6_mc(:, 6);
        md6_sig = p_md6 <= EXPERIMENT.analysis.alpha.threshold;
        eps_p_md6 = eps(p_md6);
        
        fprintf(fid, '%s & -- & %5.4f & %5.4f & %d & %d & %d & -- & -- & -- & -- & -- & -- & -- & -- & -- & --  \\\\ \n', ...
            upper(MD6), md6_soa.omega2p.factorA, md6_pwr.factorA.power, sum(md6_sig), sum(~md6_sig), length(md6_me.factorA.tg));
        
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md1) + eps(eps_p_md6);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD1), sum(md1_sig & ~md6_sig), sum(~md1_sig & md6_sig), sum(md1_sig & md6_sig), sum(~md1_sig & ~md6_sig), ...
            sum(p_md6 - p_md1 > diffeps), sum(p_md1 - p_md6 > diffeps), sum(abs(p_md6 - p_md1) <= diffeps), ...
            length(setdiff(md6_me.factorA.tg, md1_me.factorA.tg)), length(setdiff(md1_me.factorA.tg, md6_me.factorA.tg)), length(intersect(md1_me.factorA.tg, md6_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md1_sig & ~md6_sig) + sum(~md1_sig & md6_sig) + sum(md1_sig & md6_sig) + sum(~md1_sig & ~md6_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD6, MD1, size(md1_mc, 1), sum(md1_sig & ~md6_sig) + sum(~md1_sig & md6_sig) + sum(md1_sig & md6_sig) + sum(~md1_sig & ~md6_sig));
        assert(size(md1_mc, 1) ==  sum(p_md6 - p_md1 > diffeps) + sum(p_md1 - p_md6 > diffeps) + sum(abs(p_md6 - p_md1) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD6, MD1, size(md1_mc, 1), sum(p_md6 - p_md1 > diffeps) + sum(p_md1 - p_md6 > diffeps) + sum(abs(p_md6 - p_md1) <= diffeps));

        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md2) + eps(eps_p_md6);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD2), sum(md2_sig & ~md6_sig), sum(~md2_sig & md6_sig), sum(md2_sig & md6_sig), sum(~md2_sig & ~md6_sig), ...
            sum(p_md6 - p_md2 > diffeps), sum(p_md2 - p_md6 > diffeps), sum(abs(p_md6 - p_md2) <= diffeps), ...
            length(setdiff(md6_me.factorA.tg, md2_me.factorA.tg)), length(setdiff(md2_me.factorA.tg, md6_me.factorA.tg)), length(intersect(md2_me.factorA.tg, md6_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md2_sig & ~md6_sig) + sum(~md2_sig & md6_sig) + sum(md2_sig & md6_sig) + sum(~md2_sig & ~md6_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD6, MD2, size(md1_mc, 1), sum(md2_sig & ~md6_sig) + sum(~md2_sig & md6_sig) + sum(md2_sig & md6_sig) + sum(~md2_sig & ~md6_sig));
        assert(size(md1_mc, 1) ==  sum(p_md6 - p_md2 > diffeps) + sum(p_md2 - p_md6 > diffeps) + sum(abs(p_md6 - p_md2) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD6, MD2, size(md1_mc, 1), sum(p_md6 - p_md2 > diffeps) + sum(p_md2 - p_md6 > diffeps) + sum(abs(p_md6 - p_md2) <= diffeps));
        
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md3) + eps(eps_p_md6);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD3), sum(md3_sig & ~md6_sig), sum(~md3_sig & md6_sig), sum(md3_sig & md6_sig), sum(~md3_sig & ~md6_sig), ...
            sum(p_md6 - p_md3 > diffeps), sum(p_md3 - p_md6 > diffeps), sum(abs(p_md6 - p_md3) <= diffeps), ...
            length(setdiff(md6_me.factorA.tg, md3_me.factorA.tg)), length(setdiff(md3_me.factorA.tg, md6_me.factorA.tg)), length(intersect(md3_me.factorA.tg, md6_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md3_sig & ~md6_sig) + sum(~md3_sig & md6_sig) + sum(md3_sig & md6_sig) + sum(~md3_sig & ~md6_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD6, MD3, size(md1_mc, 1), sum(md3_sig & ~md6_sig) + sum(~md3_sig & md6_sig) + sum(md3_sig & md6_sig) + sum(~md3_sig & ~md6_sig));
        assert(size(md1_mc, 1) ==  sum(p_md6 - p_md3 > diffeps) + sum(p_md3 - p_md6 > diffeps) + sum(abs(p_md6 - p_md3) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD6, MD3, size(md1_mc, 1), sum(p_md6 - p_md3 > diffeps) + sum(p_md3 - p_md6 > diffeps) + sum(abs(p_md6 - p_md3) <= diffeps));

        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md4) + eps(eps_p_md6);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD4), sum(md4_sig & ~md6_sig), sum(~md4_sig & md6_sig), sum(md4_sig & md6_sig), sum(~md4_sig & ~md6_sig), ...
            sum(p_md6 - p_md4 > diffeps), sum(p_md4 - p_md6 > diffeps), sum(abs(p_md6 - p_md4) <= diffeps), ...
            length(setdiff(md6_me.factorA.tg, md4_me.factorA.tg)), length(setdiff(md4_me.factorA.tg, md6_me.factorA.tg)), length(intersect(md4_me.factorA.tg, md6_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md4_sig & ~md6_sig) + sum(~md4_sig & md6_sig) + sum(md4_sig & md6_sig) + sum(~md4_sig & ~md6_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD6, MD4, size(md1_mc, 1), sum(md4_sig & ~md6_sig) + sum(~md4_sig & md6_sig) + sum(md4_sig & md6_sig) + sum(~md4_sig & ~md6_sig));
        assert(size(md1_mc, 1) ==  sum(p_md6 - p_md4 > diffeps) + sum(p_md4 - p_md6 > diffeps) + sum(abs(p_md6 - p_md4) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD6, MD4, size(md1_mc, 1), sum(p_md6 - p_md4 > diffeps) + sum(p_md4 - p_md6 > diffeps) + sum(abs(p_md6 - p_md4) <= diffeps));
       
        fprintf(fid, '\\cline{2-17} \n');
                
        diffeps = eps(eps_p_md5) + eps(eps_p_md6);
        fprintf(fid, ' & %s & -- & -- & -- & -- & -- & %d & %d & %d & %d & %d & %d & %d & %d & %d & %d  \\\\ \n', ...
            upper(MD5), sum(md5_sig & ~md6_sig), sum(~md5_sig & md6_sig), sum(md5_sig & md6_sig), sum(~md5_sig & ~md6_sig), ...
            sum(p_md6 - p_md5 > diffeps), sum(p_md5 - p_md6 > diffeps), sum(abs(p_md6 - p_md5) <= diffeps), ...
            length(setdiff(md6_me.factorA.tg, md5_me.factorA.tg)), length(setdiff(md5_me.factorA.tg, md6_me.factorA.tg)), length(intersect(md5_me.factorA.tg, md6_me.factorA.tg)));
                
        assert(size(md1_mc, 1) == sum(md5_sig & ~md6_sig) + sum(~md5_sig & md6_sig) + sum(md5_sig & md6_sig) + sum(~md5_sig & ~md6_sig), '%s vs %s sig changes counts are not equal: %d vs %d', MD6, MD5, size(md1_mc, 1), sum(md5_sig & ~md6_sig) + sum(~md5_sig & md6_sig) + sum(md5_sig & md6_sig) + sum(~md5_sig & ~md6_sig));
        assert(size(md1_mc, 1) ==  sum(p_md6 - p_md5 > diffeps) + sum(p_md5 - p_md6 > diffeps) + sum(abs(p_md6 - p_md5) <= diffeps), '%s vs %s p-value changes counts are not equal: %d vs %d', MD6, MD5, size(md1_mc, 1), sum(p_md6 - p_md5 > diffeps) + sum(p_md5 - p_md6 > diffeps) + sum(abs(p_md6 - p_md5) <= diffeps));
        
        fprintf(fid, '\\hline \\hline \n');
        
        fprintf(fid, '\\end{tabular} \n');
        
        fprintf(fid, '\\end{table*} \n\n');
        
        
    end
                                       
    fprintf(fid, '\\end{document} \n\n');
        
    fclose(fid);
                       
    fprintf('\n\n######## Total elapsed time for reporting summary ANOVA analyses (%s): %s ########\n\n', ...
            EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));

        
end
