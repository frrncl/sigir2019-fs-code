%% rq1_plot_main_effects
% 
% Plots the main effects for the different RQ1 analyses.

%% Synopsis
%
%   [] = rq1_plot_main_effects(trackID, splitID, startMeasure, endMeasure)
%  
% *Parameters*
%
% * *|trackID|* - the identifier of the track for which the processing is
% performed.
% * *|splitID|* - the identifier of the split to process.
% * *|startMeasure|* - the index of the start measure to analyse. Optional.
% * *|endMeasure|* - the index of the end measure to analyse. Optional.
%
% *Returns*
%
% Nothing.

%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: Matlab 2015b or higher
% * *Copyright:* (C) 2016-2017 <http://ims.dei.unipd.it/ Information 
% Management Systems> (IMS) research group, <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

%%

function [] = plot_main_effects2(trackID, splitID, balanced, sstype, quartile, startMeasure, endMeasure)

     persistent RQ0 RQ1;

    if isempty(RQ0)
        RQ0 = 'rq0';
        RQ1 = 'rq1';
    end
    
 % check the number of input parameters
    narginchk(5, 7);

    % load common parameters
    common_parameters

    % check that trackID is a non-empty string
    validateattributes(trackID, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'trackID');

    if iscell(trackID)
        % check that trackID is a cell array of strings with one element
        assert(iscellstr(trackID) && numel(trackID) == 1, ...
            'MATTERS:IllegalArgument', 'Expected trackID to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    trackID = char(strtrim(trackID));
    trackID = trackID(:).';

    % check that trackID assumes a valid value
    validatestring(trackID, ...
        EXPERIMENT.track.list, '', 'trackID');

    for i = 1:length(splitID)
        
        spt = splitID{i};
        
        % check that splitID is a non-empty cell array
        validateattributes(spt, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'splitID');
        
        if iscell(spt)
            % check that splitID is a cell array of strings with one element
            assert(iscellstr(spt) && numel(spt) == 1, ...
                'MATTERS:IllegalArgument', 'Expected splitID to be a cell array of strings containing just one string.');
        end
        
        
        % check that splitID assumes a valid value
        validatestring(spt, ...
            EXPERIMENT.split.list, '', 'splitID');
        
        % check that the track and the split rely on the same corpus
        assert(strcmp(EXPERIMENT.track.(trackID).corpus, EXPERIMENT.split.(spt).corpus), 'Track %s and split %s do not rely on the same corpus', trackID, spt);
        
        
    end
            
     % check that balanced is a non-empty cell array
    validateattributes(balanced, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'balanced');
        
    if iscell(balanced)
        % check that balanced is a cell array of strings with one element
        assert(iscellstr(balanced) && numel(balanced) == 1, ...
            'MATTERS:IllegalArgument', 'Expected balanced to be a cell array of strings containing just one string.');
    end
    
    % remove useless white spaces, if any, and ensure it is a char row
    balanced = char(strtrim(balanced));
    balanced = balanced(:).';
    
    % check that balanced assumes a valid value
    validatestring(balanced, ...
        EXPERIMENT.analysis.balanced.list, '', 'balanced');
    
    % check that sstype is an integer with possible values 1, 2, 3. See
    % anovan for more details.
    validateattributes(sstype, {'numeric'}, ...
        {'nonempty', 'integer', 'scalar', '>=', 1, '<=', 3}, '', 'sstype');

    % check that quartile is a non-empty cell array
    validateattributes(quartile, {'char', 'cell'}, {'nonempty', 'vector'}, '', 'quartile');

    if iscell(quartile)
        % check that quartile is a cell array of strings with one element
        assert(iscellstr(quartile) && numel(quartile) == 1, ...
            'MATTERS:IllegalArgument', 'Expected quartile to be a cell array of strings containing just one string.');
    end

    % remove useless white spaces, if any, and ensure it is a char row
    quartile = char(strtrim(quartile));
    quartile = quartile(:).';

    % check that quartile assumes a valid value
    validatestring(quartile, ...
        EXPERIMENT.analysis.quartile.list, '', 'quartile');


    if nargin == 7
        validateattributes(startMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', 1, '<=', EXPERIMENT.measure.number }, '', 'startMeasure');

        validateattributes(endMeasure, {'numeric'}, ...
            {'nonempty', 'integer', 'scalar', '>=', startMeasure, '<=', EXPERIMENT.measure.number }, '', 'endMeasure');
    else
        startMeasure = 1;
        endMeasure = EXPERIMENT.measure.number;
    end

    % start of overall computations
    startComputation = tic;
            
    fprintf('\n\n######## Plotting main effects of ANOVA analyses on track %s (%s) ########\n\n', ...
        trackID, EXPERIMENT.label.paper);

    fprintf('+ Settings\n');
    fprintf('  - computed on %s\n', datestr(now, 'yyyy-mm-dd at HH:MM:SS'));
    fprintf('  - analysis type:\n');
    fprintf('    * balanced: %s\n', balanced);
    fprintf('    * sstype: %d\n', sstype);
    fprintf('    * quartile: %s - %s \n', quartile, EXPERIMENT.analysis.quartile.(quartile).description);
    fprintf('    * significance level alpha: %3.2f\n', EXPERIMENT.analysis.alpha.threshold);
    fprintf('  - track: %s\n', trackID);
    fprintf('  - split: %s\n', join(string(splitID), ", "));
    fprintf('  - measures:\n');
    fprintf('    * start measure: %d (%s)\n', startMeasure, EXPERIMENT.measure.getAcronym(startMeasure));
    fprintf('    * end measure: %d (%s)\n', endMeasure, EXPERIMENT.measure.getAcronym(endMeasure));            

    
     % for each measure
    for m = startMeasure:endMeasure
        
        fprintf('\n+ Plotting effects of %s\n', EXPERIMENT.measure.getAcronym(m));
        
        mid = EXPERIMENT.measure.list{m};
                
        % whole corpus
        anovaID = EXPERIMENT.pattern.identifier.anova.analysis(RQ0, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID{1}).corpus, trackID);
        anovaMeID = EXPERIMENT.pattern.identifier.anova.me(RQ0, 'b', sstype, quartile, mid, EXPERIMENT.split.(splitID{1}).corpus, trackID);                
        serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
                'WorkspaceVarNames', {'rq0_me'}, ...
                'FileVarNames', {anovaMeID});
            
        rq1_me = cell(1, length(splitID));
        
        % shards
        for i = 1:length(splitID)
            anovaID = EXPERIMENT.pattern.identifier.anova.analysis(RQ1, balanced, sstype, quartile, mid, splitID{i}, trackID);
            anovaMeID = EXPERIMENT.pattern.identifier.anova.me(RQ1, balanced, sstype, quartile, mid, splitID{i}, trackID);
        
             serload2(EXPERIMENT.pattern.file.analysis(trackID, anovaID), ...
            'WorkspaceVarNames', {'tmp'}, ...
            'FileVarNames', {anovaMeID});        
            
            rq1_me{i} = tmp;
        end
            
            
        currentFigure = figure('Visible', 'off');
        
            %--------------------------------------------------------------
            % topic/system on whole corpus
            xTick = 1:length(rq0_me.factorA.label);
            data.mean = rq0_me.factorA.mean.';
            data.ciLow = rq0_me.factorA.ci(:, 1).';
            data.ciHigh = rq0_me.factorA.ci(:, 2).';        

            plot(xTick, data.mean, 'Color', rgb('Black'), 'LineWidth', 3, 'LineStyle', '--');

            hold on

            hFill = fill([xTick fliplr(xTick)],[data.ciHigh fliplr(data.ciLow)], rgb('Black'), ...
                        'LineStyle', 'none', 'EdgeAlpha', 0.25, 'FaceAlpha', 0.25);

            % send the fill to back
            uistack(hFill, 'bottom');

            % Exclude fill from legend
            set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');

            lbl = cell(1, length(splitID));
                    
            for i = 1:length(splitID)
                
                %--------------------------------------------------------------
                % topic/system on sub-corpora
                tmp = rq1_me{i};
                                
                xTick = 1:length(tmp.factorA.label);
                data.mean = tmp.factorA.mean.';
                data.ciLow = tmp.factorA.ci(:, 1).';
                data.ciHigh = tmp.factorA.ci(:, 2).';
                
                plot(xTick, data.mean, 'Color', EXPERIMENT.split.(splitID{i}).color, 'LineWidth', 3);
                
                hold on
                
                hFill = fill([xTick fliplr(xTick)],[data.ciHigh fliplr(data.ciLow)], EXPERIMENT.split.(splitID{i}).color, ...
                    'LineStyle', 'none', 'EdgeAlpha', 0.25, 'FaceAlpha', 0.25);
                
                % send the fill to back
                uistack(hFill, 'bottom');
                
                % Exclude fill from legend
                set(get(get(hFill, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle','off');
                
                lbl{i} = sprintf('%s (\\tau = %5.4f)',  EXPERIMENT.split.(splitID{i}).label, rq1_me{i}.factorA.tau);
                                
            end
            
            
            legend([{'Whole corpus'} lbl]);
            
            ax = gca;
            ax.TickLabelInterpreter = 'latex';
            ax.FontSize = 24;

            ax.XTick = [];
            ax.XTickLabel = [];
            %ax1.XTickLabelRotation = 90;

            ax.XLabel.Interpreter = 'latex';
            ax.XLabel.String = sprintf('\\texttt{%s} Systems', trackID);

            ax.YLabel.Interpreter = 'latex';
            ax.YLabel.String = sprintf('%s Marginal Mean', EXPERIMENT.measure.(mid).acronym);
            
            ax.XLim = [0 length(rq0_me.factorA.label)];
            ax.YLim = [0 ax.YLim(2)];

           % title(sprintf('Correlation between rankings of systems on whole collection and shards $\\tau$ = %5.4f', ), 'FontSize', 24, 'Interpreter', 'latex');            
            
        currentFigure.PaperPositionMode = 'auto';
        currentFigure.PaperUnits = 'centimeters';
        currentFigure.PaperSize = [42 17];
        currentFigure.PaperPosition = [1 1 40 15];

        figureID = EXPERIMENT.pattern.identifier.figure.me(balanced, sstype, quartile, join(string(splitID), "_"), trackID);

        print(currentFigure, '-dpdf', EXPERIMENT.pattern.file.figure(trackID, figureID));

        close(currentFigure)
    
    end % measure
           
    fprintf('\n\n######## Total elapsed time for plotting main effects on track %s (%s): %s ########\n\n', ...
           EXPERIMENT.track.(trackID).name, EXPERIMENT.label.paper, elapsedToHourMinutesSeconds(toc(startComputation)));
       
end

